<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'customer';

    static function insert($data, $request)
    {
        $data->nama = $request->nama;
        $data->slug = str_shuffle(preg_replace('/\s+/', '-', str_replace('/', '-', strtolower($request->nama))).$request->telepon);
        $data->telepon = $request->telepon;
        $data->kelurahan_id = $request->kelurahan;
        $data->alamat = $request->alamat;
        $data->save();
    }

    static function del($data, $slug)
    {
        $data->where('slug', $slug)->delete();
    }

    public function kelurahan()
    {
        return $this->belongsTo('App\Models\Kelurahan', 'kelurahan_id', 'id');
    }
}
