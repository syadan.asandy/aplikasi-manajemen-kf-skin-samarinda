<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RincianTransaksi extends Model
{
    use HasFactory;

    protected $table =  'rincian_transaksi';

    public $timestamps = false;

    public function produk()
    {
        return $this->belongsTo('App\Models\Produk', 'produk_id', 'id');
    }
}
