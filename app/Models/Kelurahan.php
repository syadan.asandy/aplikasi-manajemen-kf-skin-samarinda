<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Kelurahan extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = "Kelurahan";

    public function kabupaten()
    {
        return $this->hasOne('App\Models\Kabupaten', 'id', 'kabupaten_id');
    }
}
