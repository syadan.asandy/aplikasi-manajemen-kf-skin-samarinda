<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Produk extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'produk';

    static function insert($data, $request)
    {
        if ($request->foto != null) {
            $data->foto = $request->foto;
        }

        $data->nama = $request->nama;
        $data->slug = preg_replace('/\s+/', '-', str_replace('/', '-', strtolower($request->nama)));
        $data->harga_beli = str_replace('.', '', $request->harga_beli);
        $data->harga = str_replace('.', '', $request->harga);
        $data->save();
    }

    static function del($data, $slug)
    {
        $data->where('slug', $slug)->delete();
    }
}
