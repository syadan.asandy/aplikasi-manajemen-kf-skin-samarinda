<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Kabupaten extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = "Kabupaten";

    public function kelurahan()
    {
        return $this->hasMany('App\Models\Kelurahan', 'id', 'kabupaten_id');
    }
}
