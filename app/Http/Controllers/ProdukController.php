<?php

namespace App\Http\Controllers;

use App\Models\Produk;
use Illuminate\Http\Request;
use App\Http\Requests\CreateProdukRequest;

class ProdukController extends Controller
{
    public $produkModel;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    function __construct()
    {
        $this->produkModel = new Produk();
    }

    public function index()
    {
        return view('produk.index', ['data' => $this->produkModel->all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('produk.create', ['submitbuttontext' => 'Tambah']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(CreateProdukRequest $request)
    {
        Produk::insert($this->produkModel, $request);

        return redirect()->route('produk.index')->with('success', 'Produk berhasil dibuat.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Produk  $produk
     * @return \Illuminate\Http\Response
     */
    public function show(Produk $produk)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Produk  $produk
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        return view('produk.edit', ['data' => $this->produkModel->where('slug', $slug)->first(), 'submitbuttontext' => 'Ubah']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Produk  $produk
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug)
    {
        Produk::insert($this->produkModel->where('slug', $slug)->first(), $request);

        return redirect()->route('produk.index')->with('warning', 'Produk berhasil diubah.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Produk  $produk
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
        Produk::del($this->produkModel, $slug);
        return back()->with('danger', 'Produk berhasil dihapus');
    }
}
