<?php

namespace App\Http\Controllers;

use Auth;
use App\Exports\TransaksiBulananExport;
use App\Models\Transaksi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class LaporanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $bulan = [
            [
                'nama' => 'Januari',
                'angka' => '01'
            ],
            [
                'nama' => 'Februari',
                'angka' => '02'
            ],
            [
                'nama' => 'Maret',
                'angka' => '03'
            ],
            [
                'nama' => 'April',
                'angka' => '04'
            ],
            [
                'nama' => 'Mei',
                'angka' => '05'
            ],
            [
                'nama' => 'Juni',
                'angka' => '06'
            ],
            [
                'nama' => 'Juli',
                'angka' => '07'
            ],
            [
                'nama' => 'Agustus',
                'angka' => '08'
            ],
            [
                'nama' => 'September',
                'angka' => '09'
            ],
            [
                'nama' => 'Oktober',
                'angka' => '10'
            ],
            [
                'nama' => 'November',
                'angka' => '11'
            ],
            [
                'nama' => 'Desember',
                'angka' => '12'
            ],
        ];

        $tahun = Transaksi::select('created_at')->orderBy('created_at', 'asc')->get()
                    ->groupBy(function($date) {
                        return \Carbon\Carbon::parse($date->created_at)->format('Y'); // grouping by years
                        // return \Carbon\Carbon::parse($date->created_at)->format('m'); // grouping by months
                    });
        return view('laporan.index', ['bulan' => $bulan, 'tahun' => $tahun]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // penyalahan method
        // kujadikan route show view laporan
        if (isset($request->bulan)) {
            return view('laporan.LaporanRinci', ['data' => Transaksi::where('created_at', 'like', $request->tahun.'-'.$request->bulan.'-%')->orderBy('created_at', 'asc')->get(), 'laporan' => 'bulanan', 'bulan' => LaporanController::bulan($request->bulan), 'tahun' => $request->tahun, 'angkabulan' => $request->bulan]);
        }
        return view('laporan.LaporanRinci', ['data' => Transaksi::where('created_at', 'like', $request->tahun.'-%')->orderBy('created_at', 'asc')->get(), 'laporan' => 'tahunan', 'tahun' => $request->tahun]);
    }

    static function bulan($bulan)
    {
        switch ($bulan) {
            case '01':
                return 'Januari';
            case '02':
                return 'Februari';
            case '03':
                return 'Maret';
            case '04':
                return 'April';
            case '05':
                return 'Mei';
            case '06':
                return 'Juni';
            case '07':
                return 'Juli';
            case '08':
                return 'Agustus';
            case '09':
                return 'September';
            case '10':
                return 'Oktober';
            case '11':
                return 'November';
            case '12':
                return 'Desember';

            default:
                # code...
                break;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Transaksi  $transaksi
     * @return \Illuminate\Http\Response
     */
    public function show(Transaksi $transaksi)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Transaksi  $transaksi
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaksi $transaksi)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Transaksi  $transaksi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $status)
    {
        if ($status === 'bulanan') {
            // return DB::select('select * from transaksi where created_at like \''.$request->tahun.'-'.$request->bulan.'%\'');
            // $a = new TransaksiBulananExport($request->bulan, $request->tahun);
            // return $a->collection();
            return Excel::download(new TransaksiBulananExport($request->bulan, $request->tahun), 'ReportBulanan-'.$request->tahun.'-'.$request->bulan.'-'.Auth::user()->name.'.xlsx');
        }
        else {
            // return DB::select('select * from transaksi where created_at like \''.$request->tahun.'%\'');
            return Excel::download(new TransaksiBulananExport($request->bulan, $request->tahun), 'ReportTahunan-'.$request->tahun.'-'.Auth::user()->name.'.xlsx');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Transaksi  $transaksi
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaksi $transaksi)
    {

    }
}
