<?php

namespace App\Http\Controllers;

use App\Models\Kabupaten;
use Illuminate\Http\Request;

class KabupatenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('kabupaten.index', ['data' => Kabupaten::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('kabupaten.create', ['submitbuttontext' => 'Tambah']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $newData = new Kabupaten();
        $newData->nama = $request->nama;
        $newData->save();

        return redirect()->route('kabupaten.index')->with('success', 'Kabupaten '. $newData->nama .' berhasil dibuat.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Kabupaten  $kabupaten
     * @return \Illuminate\Http\Response
     */
    public function show(Kabupaten $kabupaten)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Kabupaten  $kabupaten
     * @return \Illuminate\Http\Response
     */
    public function edit($nama)
    {
        return view('kabupaten.edit', ['data' => Kabupaten::where('nama', $nama)->first(), 'submitbuttontext' => 'Ubah']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Kabupaten  $kabupaten
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $oldData = Kabupaten::where('id', $id)->first();
        $nama = $oldData->nama;
        $oldData->nama = $request->nama;
        $oldData->save();

        return redirect()->route('kabupaten.index')->with('success', 'Kabupaten '. $nama .' menjadi '. $oldData->nama .' berhasil diubah.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Kabupaten  $kabupaten
     * @return \Illuminate\Http\Response
     */
    public function destroy($nama)
    {
        $data = Kabupaten::where('nama', $nama)->first();
        $data->delete();

        return back()->with('danger', 'Kabupaten '. $nama .' berhasil dihapus.');
    }
}
