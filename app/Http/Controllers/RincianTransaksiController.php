<?php

namespace App\Http\Controllers;

use App\Models\Produk;
use App\Models\RincianTransaksi;
use Illuminate\Http\Request;

class RincianTransaksiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $idTransaksi)
    {
        return $request;
    }

    static function insert($request, $idTransaksi)
    {
        for ($i=0; $i < count($request->produk); $i++) {
            $rinci = new RincianTransaksi();
            $produk = Produk::where('id', $request->produk[$i])->first();
            $rinci->qty = $request->qty[$i];
            $rinci->harga = $produk->harga;
            $rinci->diskon = $request->diskon[$i];
            $rinci->produk_id = $produk->id;
            $rinci->transaksi_id = $idTransaksi->id;
            $rinci->save();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\rincianTransaksi  $rincianTransaksi
     * @return \Illuminate\Http\Response
     */
    public function show(rincianTransaksi $rincianTransaksi)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\rincianTransaksi  $rincianTransaksi
     * @return \Illuminate\Http\Response
     */
    public function edit(rincianTransaksi $rincianTransaksi)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\rincianTransaksi  $rincianTransaksi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, rincianTransaksi $rincianTransaksi)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\rincianTransaksi  $rincianTransaksi
     * @return \Illuminate\Http\Response
     */
    public function destroy(rincianTransaksi $rincianTransaksi)
    {
        //
    }
}
