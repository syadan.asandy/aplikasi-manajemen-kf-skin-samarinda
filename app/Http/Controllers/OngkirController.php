<?php

namespace App\Http\Controllers;

use App\Models\Kelurahan;
use Illuminate\Http\Request;

class OngkirController extends Controller
{
    public function index()
    {
        return view('ongkir.index', ['data' => Kelurahan::all()]);
    }

    public function create()
    {

    }

    public function store()
    {

    }

    public function edit($nama)
    {
        return view('ongkir.edit', ['data' => Kelurahan::where('nama', $nama)->first(), 'submitbuttontext' => 'Ubah data']);
    }

    public function update(Request $request, $nama)
    {
        $oldOngkir = Kelurahan::where('nama', $nama)->first();
        $oldOngkir->ongkir = str_replace('.', '', $request->ongkir);
        $oldOngkir->save();

        return redirect()->route('ongkir.index')->with('success', 'Ongkir '. $oldOngkir->nama .' berhasil di update.');
    }

    public function delete()
    {

    }
}
