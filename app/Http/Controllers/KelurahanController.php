<?php

namespace App\Http\Controllers;

use App\Models\Kabupaten;
use App\Models\Kelurahan;
use Illuminate\Http\Request;

class KelurahanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('kelurahan.index', ['data' => Kelurahan::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('kelurahan.create', ['kabupaten' => Kabupaten::all(),'submitbuttontext' => 'Tambah']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $newData = new Kelurahan();
        $newData->nama = $request->nama;
        $newData->kabupaten_id = $request->kabupaten_id;
        $newData->save();

        return redirect()->route('kelurahan.index')->with('success', 'Kelurahan '. $newData->nama .' berhasil dibuat.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\kelurahan  $kelurahan
     * @return \Illuminate\Http\Response
     */
    public function show(kelurahan $kelurahan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\kelurahan  $kelurahan
     * @return \Illuminate\Http\Response
     */
    public function edit($nama)
    {
        return view('kelurahan.edit', ['kabupaten' => Kabupaten::all(), 'data' => Kelurahan::where('nama', $nama)->first(), 'submitbuttontext' => 'Ubah']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\kelurahan  $kelurahan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $oldData = Kelurahan::where('nama', $id)->first();
        $nama = $oldData->nama;
        $oldData->nama = $request->nama;
        $oldData->kabupaten_id = $request->kabupaten_id;
        $oldData->save();

        return redirect()->route('kelurahan.index')->with('success', 'Kelurahan '. $nama .' menjadi '. $oldData->nama .' berhasil diubah.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\kelurahan  $kelurahan
     * @return \Illuminate\Http\Response
     */
    public function destroy($nama)
    {
        $data = Kelurahan::where('nama', $nama)->first();
        $data->delete();

        return back()->with('danger', 'Kelurahan '. $nama .' berhasil dihapus.');
    }
}
