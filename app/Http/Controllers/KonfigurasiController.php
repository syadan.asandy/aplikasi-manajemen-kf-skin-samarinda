<?php

namespace App\Http\Controllers;

use App\Models\Konfigurasi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class KonfigurasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('konfigurasi.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = Konfigurasi::where('id', $request->id)->first();

        if ($request->formstatus == 'about') {
            if ($request->hasFile('foto')) {
                $image = $request->file('foto');
                if ($data->profile_photo_path != null) {
                    unlink('public/img/users/'.$data->profile_photo_path);
                }
                $name = $data->id.'.'.$image->getClientOriginalExtension();
                $data->profile_photo_path = $name;
                $data->save();
                $image->move('public/img/users/', $name);
            }

            $data->name = $request->nama;
            $data->telepon = $request->telepon;
            $data->kabupaten = $request->kabupaten;
            $data->kecamatan = $request->kecamatan;
            $data->alamat = $request->alamat;
            $data->save();
        }
        elseif ($request->formstatus == 'web') {
            if ($request->hasFile('logo')) {
                $image = $request->file('logo');
                if ($data->logo_web != null) {
                    unlink('public/img/users/'.$data->logo_web);
                }
                $name = 'logoweb-'.$data->id.'.'.$image->getClientOriginalExtension();
                $data->logo_web = $name;
                $data->save();
                $image->move('public/img/users/', $name);
            }

            if ($request->hasFile('logonota')) {
                $image = $request->file('logonota');
                if ($data->logo_nota != null) {
                    unlink('public/img/users/'.$data->logo_nota);
                }
                $name = 'logonota-'.$data->id.'.'.$image->getClientOriginalExtension();
                $data->logo_nota = $name;
                $data->save();
                $image->move('public/img/users/', $name);
            }

            if ($request->hasFile('bg_nota')) {
                $image = $request->file('bg_nota');
                if ($data->bg_nota != null) {
                    unlink('public/img/users/'.$data->bg_nota);
                }
                $name = 'bg_nota-'.$data->id.'.'.$image->getClientOriginalExtension();
                $data->bg_nota = $name;
                $data->save();
                $image->move('public/img/users/', $name);
            }

            $data->status_nota = $request->statusnota;
            $data->text_nota = $request->text_nota;
            $data->save();
        }
        elseif ($request->formstatus == 'password') {
            $request->validate([
                'confNewPassword' => 'min:8|alpha_num|same:newPassword',
            ]);

            if (preg_match('~[0-9]+~', $request->confNewPassword)) {
                if (password_verify($request->oldPassword, $data->password)) {
                    $data->password = Hash::make($request->newPassword);
                    $data->save();
                    return back()->with('success', 'Password anda berhasil di update');
                }
                return back()->with('danger', 'Password anda salah!!');
            }
            return back()->with('danger', 'Password harus terdiri dari angka dan huruf!!');
        }
        else {
            echo "404";
        }

        return redirect()->route('konfigurasi.index')->with('success', 'Berhasil Update Data!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Konfigurasi  $konfigurasi
     * @return \Illuminate\Http\Response
     */
    public function show(Konfigurasi $konfigurasi)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Konfigurasi  $konfigurasi
     * @return \Illuminate\Http\Response
     */
    public function edit(Konfigurasi $konfigurasi)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Konfigurasi  $konfigurasi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $status, $id)
    {
        if ($status == 'about') {
            echo "about";
        }
        elseif ($status == 'web') {
            echo "web";
        }
        elseif ($status == 'password') {
            echo "password";
        }
        else {
            echo "404";
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Konfigurasi  $konfigurasi
     * @return \Illuminate\Http\Response
     */
    public function destroy(Konfigurasi $konfigurasi)
    {
        //
    }
}
