<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCostumerRequest;
use App\Models\Customer;
use App\Models\Kabupaten;
use App\Models\Kelurahan;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public $customerModel;

    function __construct()
    {
        $this->customerModel = new Customer();
    }

    public function index()
    {
        return view('customer.index', ['data' => $this->customerModel->all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('customer.create', ['submitbuttontext' => 'Tambah', 'kelurahan' => Kelurahan::where('kabupaten_id', '1')->get(), 'kelurahan1' => Kelurahan::where('kabupaten_id', '2')->get(), 'kabupaten' => Kabupaten::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCostumerRequest $request)
    {
        $request->validate([
            'telepon' => 'digits_between:11,13',
        ]);

        Customer::insert($this->customerModel, $request);
        return redirect()->route('customer.index')->with('success', 'Customer berhasil dibuat.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show(Customer $customer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        return view('customer.edit', ['data' => $this->customerModel->where('slug', $slug)->first(), 'submitbuttontext' => 'Tambah', 'kelurahan' => Kelurahan::where('kabupaten_id', '1')->get(), 'kelurahan1' => Kelurahan::where('kabupaten_id', '2')->get(), 'kabupaten' => Kabupaten::all()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug)
    {
        Customer::insert($this->customerModel->where('slug', $slug)->first(), $request);
        return redirect()->route('customer.index')->with('warning', 'Customer berhasil diubah.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
        Customer::del($this->customerModel, $slug);
        return back()->with('danger', 'Customer berhasil dihapus.');
    }
}
