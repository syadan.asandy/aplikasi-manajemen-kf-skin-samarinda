<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Produk;
use App\Models\Transaksi;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;

class TransaksiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $transaksiModel;

    function __construct()
    {
        $this->transaksiModel = new Transaksi();
    }

    public function index()
    {
        return view('transaksi.index', ['data' => $this->transaksiModel->orderBy('created_at', 'desc')->get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $customer = Customer::select('id', 'nama')->get();
        // $array = array();
        // foreach ($customer as $each) {
        //     $array[$each->id] = $each->nama;
        // }

        // $produk = Produk::select('id', 'nama', 'harga')->get();
        // $array1 = array();
        // foreach ($produk as $each) {
        //     $array1[$each->id] = $each->nama;
        // }
        return view('transaksi.create', ['customer' => Customer::all(), 'produk' => Produk::all(), 'submitbuttontext' => 'Checkout']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $total = 0;
        for ($i=0; $i < count($request->produk); $i++) {
            $produk = Produk::where('id', $request->produk[$i])->first();
            $total += ($produk->harga-$request->diskon[$i]) * $request->qty[$i];
        }

        $newTransaksi = new Transaksi();
        $newTransaksi->nomor = time()."0".count(Transaksi::all());
        $newTransaksi->customer_id = $request->customer;
        $newTransaksi->user_id = $request->user_id;
        $newTransaksi->total = $total+$request->harga;
        $newTransaksi->ongkir = $request->harga;
        $newTransaksi->created_at = now();
        $newTransaksi->updated_at = now();
        $newTransaksi->save();

        RincianTransaksiController::insert($request, Transaksi::select('id')->where('nomor', $newTransaksi->nomor)->first());

        // $data = Transaksi::where('nomor', $newTransaksi->nomor)->first();
        // $pdf = PDF::loadView('transaksi.cetakNota', ['data' => $data])->setPaper('A4', 'landscape');

        // $pdf->save(public_path().'\\transaksiFile\\nota\\'.$newTransaksi->nomor.'.pdf',$pdf);


        return redirect()->route('transaksi.index')->with('success', 'Transaksi berhasil dibuat.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Transaksi  $transaksi
     * @return \Illuminate\Http\Response
     */
    public function show(Transaksi $transaksi)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Transaksi  $transaksi
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaksi $transaksi)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Transaksi  $transaksi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaksi $transaksi)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Transaksi  $transaksi
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaksi $transaksi)
    {
        //
    }

    public function cetak($nomor)
    {
        $data = Transaksi::where('nomor', $nomor)->first();
        // return view('transaksi.cetakNota', compact('data'));

        $pdf = PDF::loadView('transaksi.cetakNota', ['data' => $data])->setPaper('A4','landscape');
        // set_time_limit(300);
        // return public_path();
        // $pdf->save(public_path().'\\transaksiFile\\nota\\'.$nomor.'.pdf',$pdf);
        return $pdf->stream($nomor.'.pdf');

        // return $data;
    }
}
