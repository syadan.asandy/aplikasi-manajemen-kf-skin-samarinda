<?php

namespace App\Http\Controllers;

use App\Models\KelurahanLuarSamarinda;
use Illuminate\Http\Request;

class KelurahanLuarSamarindaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\KelurahanLuarSamarinda  $kelurahanLuarSamarinda
     * @return \Illuminate\Http\Response
     */
    public function show(KelurahanLuarSamarinda $kelurahanLuarSamarinda)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\KelurahanLuarSamarinda  $kelurahanLuarSamarinda
     * @return \Illuminate\Http\Response
     */
    public function edit(KelurahanLuarSamarinda $kelurahanLuarSamarinda)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\KelurahanLuarSamarinda  $kelurahanLuarSamarinda
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, KelurahanLuarSamarinda $kelurahanLuarSamarinda)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\KelurahanLuarSamarinda  $kelurahanLuarSamarinda
     * @return \Illuminate\Http\Response
     */
    public function destroy(KelurahanLuarSamarinda $kelurahanLuarSamarinda)
    {
        //
    }
}
