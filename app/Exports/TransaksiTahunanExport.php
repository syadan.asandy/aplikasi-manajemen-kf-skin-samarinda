<?php

namespace App\Exports;

use App\Models\Transaksi;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class TransaksiTahunanExport implements FromCollection, WithHeadings, ShouldAutoSize, WithStyles, WithEvents
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public $date;

    function __construct($date)
    {
        $this->date = $date;
    }

    public function collection()
    {
        // return Transaksi::all();

        return Transaksi::select('transaksi.nomor as NOMOR TRANSAKSI', 'customer.nama as CUSTOMER', 'customer.alamat as ALAMAT', 'customer.telepon as TELEPON', 'transaksi.total as TOTAL', 'users.name as ADMIN', 'transaksi.created_at as TANGGAL')
            ->join('customer', 'customer.id', '=', 'transaksi.customer_id')
            ->join('users', 'users.id', '=', 'transaksi.user_id')
            ->where('transaksi.created_at', 'like', $this->date.'%')->orderBy('transaksi.created_at','asc')->get();
        // return json_encode(DB::select('select * from transaksi where created_at like \''.$this->year.'-'.$this->month.'%\''));
    }

    public function headings(): array
    {
        return [
            'NOMOR TRANSAKSI',
            'CUSTOMER',
            'ALAMAT',
            'TELEPON',
            'TOTAL',
            'ADMIN',
            'TANGGAL',
        ];
    }

    public function styles(Worksheet $sheet)
    {
        $sheet->getStyle('A1:W1')->getFont()->setBold(true)->setSize(12);
        // $sheet->setBorderStyle();
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:W1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(12);
                // $styleArray = [
                //     'borders' => [
                //         'outline' => [
                //             'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                //             'color' => ['rgb' => '#FFFF'],
                //         ],
                //     ],
                // ];
                $styleArray = array(
                    'borders' => array(
                        'allBorders' => array(
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => '00000000'],
                        )
                    )
                );

                $event->sheet->getStyle('A1:G'.($this->collection()->count()+1))->applyFromArray($styleArray);
                // $event->sheet->getStyle('A1:W1')->applyFromArray(array('font' => [
                //     'bold' => 'bold',
                // ]));
            },
        ];
    }
}
