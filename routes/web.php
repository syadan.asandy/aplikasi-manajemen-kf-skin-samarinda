<?php
use Chartisan\PHP\Chartisan;
use App\Charts\TransaksiChart;
use Charts as ch;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    $totalProduk = count(App\Models\Produk::all());
    $totalCustomer = count(App\Models\Customer::all());
    $totalTransaksi = count(App\Models\Transaksi::all());
    $transaksi = App\Models\Transaksi::orderBy('created_at', 'desc')->get()
    ->groupBy(function($date) {
        return \Carbon\Carbon::parse($date->created_at)->format('M/y'); // grouping by years
        // return \Carbon\Carbon::parse($date->created_at)->format('m'); // grouping by months
    })->take(12);
// return $transaksi;
    $transaksiVal = array();
    $transaksiRange = array();

    foreach ($transaksi as $key => $value) {
        array_push($transaksiVal, $value->count());
        array_push($transaksiRange, $key);
    }

    $produkLabel = array();
    $produkData = array();

    $favList = DB::select('select nama, sum(rincian_transaksi.qty) as jumlah from rincian_transaksi right join produk on produk.id = rincian_transaksi.produk_id  where produk.deleted_at is null group by produk.nama order by produk.id asc');
// return $favList;

    foreach ($favList as $value) {
        array_push($produkLabel, $value->nama);
        array_push($produkData, $value->jumlah);
    }

    // return $produkData;
    return view('dashboard', ['totalProduk' => $totalProduk, 'totalCustomer' => $totalCustomer, 'totalTransaksi' => $totalTransaksi, 'transaksiVal' => $transaksiVal, 'transaksiRange' => $transaksiRange, 'produkLabel' => $produkLabel, 'produkData' => $produkData]);
})->name('dashboard');

Route::resource('produk', 'App\Http\Controllers\ProdukController')->middleware(['auth:sanctum', 'verified']);
Route::resource('customer', 'App\Http\Controllers\CustomerController')->middleware(['auth:sanctum', 'verified']);
Route::resource('transaksi', 'App\Http\Controllers\TransaksiController')->middleware(['auth:sanctum', 'verified']);
Route::post('transaksirinci/{request}/{idTransaksi}', 'App\Http\Controllers\RincianTransaksiController@store')->middleware(['auth:sanctum', 'verified'])->name('transaksirinci.store');
Route::get('transaksi/cetak-nota/{nomor}', 'App\Http\Controllers\TransaksiController@cetak')->middleware(['auth:sanctum', 'verified'])->name('transaksi.cetak');
Route::resource('laporan', 'App\Http\Controllers\LaporanController')->middleware(['auth:sanctum', 'verified']);
Route::resource('kabupaten', 'App\Http\Controllers\KabupatenController')->middleware(['auth:sanctum', 'verified']);
Route::resource('kelurahan', 'App\Http\Controllers\KelurahanController')->middleware(['auth:sanctum', 'verified']);
Route::resource('ongkir', 'App\Http\Controllers\OngkirController')->middleware(['auth:sanctum', 'verified']);
Route::resource('konfigurasi', 'App\Http\Controllers\KonfigurasiController')->middleware(['auth:sanctum', 'verified']);

// Sampah
Route::middleware(['auth:sanctum', 'verified'])->get('sampah', function(){
    $data = ($_GET['jenis'] == 'produk') ? \App\Models\Produk::onlyTrashed()->orderBy('deleted_at', 'desc')->get() : \App\Models\Customer::onlyTrashed()->orderBy('deleted_at', 'desc')->get();
    return view('sampah', ['data' => $data, 'jenis' => $_GET['jenis']]);
})->name('sampah');
Route::get('sampah/restore', function(){
    $data = ($_GET['jenis'] == 'produk') ? \App\Models\Produk::onlyTrashed()->where('id', $_GET['id'])->first() : \App\Models\Customer::onlyTrashed()->where('id', $_GET['id'])->first();
    $data->restore();
    return redirect()->route('sampah', ['jenis' => $_GET['jenis']])->with('success', ucfirst($_GET['jenis']).' '. $data->nama .' berhasil di kembalikan!');
})->name('restore.sampah')->middleware(['auth:sanctum', 'verified']);
