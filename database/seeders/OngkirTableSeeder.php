<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class OngkirTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ongkir = [10000, 15000, 25000, 30000];
        $data = \App\Models\Kelurahan::all();
        foreach ($data as $key => $value) {

            $value->ongkir = $ongkir[array_rand($ongkir)];
            $value->save();
        }
    }
}
