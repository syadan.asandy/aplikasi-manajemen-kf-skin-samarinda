<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProdukTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('produk')->insert([
            [
                'nama' => 'LC Beauty Basic Komplit',
                'slug' => 'lc-beauty-basic-komplit',
                'harga' => '315000',
                'foto' => null,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],
            [
                'nama' => 'KF Skin Basic Komplit',
                'slug' => 'kf-skin-basic-komplit',
                'harga' => '300000',
                'foto' => null,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],
            [
                'nama' => 'Night Cream LC Beauty',
                'slug' => 'night-cream-lc-beauty',
                'harga' => '120000',
                'foto' => null,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],
            [
                'nama' => 'Day Cream LC Beauty',
                'slug' => 'day-cream-lc-beauty',
                'harga' => '110000',
                'foto' => null,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],
            [
                'nama' => 'Toner LC Beauty',
                'slug' => 'toner-lc-beauty',
                'harga' => '90000',
                'foto' => null,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],
            [
                'nama' => 'Facial Wash LC Beauty',
                'slug' => 'facial-wash-lc-beauty',
                'harga' => '100000',
                'foto' => null,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],
            [
                'nama' => 'Serum Acne',
                'slug' => 'serum-acne',
                'harga' => '100000',
                'foto' => null,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],
            [
                'nama' => 'Serum Vit C Derma',
                'slug' => 'serum-vit-c-derma',
                'harga' => '130000',
                'foto' => null,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],
            [
                'nama' => 'Serum Darkspot',
                'slug' => 'serum-darkspot',
                'harga' => '140000',
                'foto' => null,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],
            [
                'nama' => 'Serum Glowing Gold',
                'slug' => 'serum-glowing-gold',
                'harga' => '150000',
                'foto' => null,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],
            [
                'nama' => 'Serum Silky',
                'slug' => 'serum-silky',
                'harga' => '160000',
                'foto' => null,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],
            [
                'nama' => 'Kapas',
                'slug' => 'kapas',
                'harga' => '5000',
                'foto' => null,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],
            [
                'nama' => 'Pouch LC Beauty',
                'slug' => 'pouch-lc-beauty',
                'harga' => '5000',
                'foto' => null,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],
        ]);
    }
}
