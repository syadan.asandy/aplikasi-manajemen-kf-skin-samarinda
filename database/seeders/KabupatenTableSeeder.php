<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class KabupatenTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'Samarinda',
            'Lain-lain',
        ];

        foreach ($data as $key => $value) {
            DB::table('kabupaten')->insert([
                [
                    'nama' => $value,
                ]
            ]);
        }
    }
}
