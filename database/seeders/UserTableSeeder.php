<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Hanie Sri Romandani',
            'email' => 'haniesr9@gmail.com',
            'password' => '$2y$10$5uXBWlTmPtQk3MuwoQoq8e15kLm7mbqylqjyTWY2RkyaveiLOZLpC',
            'profile_photo_path' => '1.jpg',
        ]);
    }
}
