<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CustomerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create('id_ID');
        $alamat = \App\Models\Kelurahan::all();

        for ($i=0; $i < 1000; $i++) {
            $nama = $faker->name;
            DB::table('customer')->insert([
                [
                    'nama' => $nama,
                    'telepon' => $faker->phoneNumber,
                    'alamat' => $faker->streetAddress,
                    'kelurahan_id' => $alamat[rand(0, $alamat->count()-1)]->id,
                    'slug' => $nama,
                    'created_at' => now(),
                    'updated_at' => now(),
                ]
            ]);
        }
    }
}
