<?php

namespace Database\Seeders;

use App\Models\Produk;
use App\Models\Customer;
use App\Models\Transaksi;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TransaksiTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $customer = Customer::all();
        $produk = Produk::all();
        $totalTransaksiVar = rand(3000,3500);
        for ($j=0; $j < $totalTransaksiVar; $j++) {
            $barang = array();
            $nomor = time()."0".$j;
            $total = 0;
            $n = rand(1,4);

            for ($i=0; $i < $n; $i++) {
                $produk1 = $produk[rand(0, 12)];
                $qty = rand(1,2);
                $total += $produk1->harga * $qty;
                array_push($barang, [
                    'qty' => $qty,
                    'harga' => $produk1->harga,
                    'produk_id' => $produk1->id,
                ]);
            }

            $ongkir = [10000, 15000, 20000];
            $ongkir = $ongkir[array_rand($ongkir)];
            $tgl = date("Y-m-d H:i:s",mt_rand(strtotime('2018-01-01'), strtotime('2022-05-01')));
            DB::table('transaksi')->insert([
                [
                    'nomor' => $nomor,
                    'total' => $total+$ongkir,
                    'customer_id' => $customer[rand(0,999)]->id,
                    'user_id' => 1,
                    'ongkir' => $ongkir,
                    'created_at' => $tgl,
                    'updated_at' => $tgl,
                ]
            ]);

            $id = Transaksi::where('nomor', $nomor)->first();

            for ($i=0; $i < $n; $i++) {
                DB::table('rincian_transaksi')->insert([
                    [
                        'qty' => $barang[$i]['qty'],
                        'harga' => $barang[$i]['harga'],
                        'produk_id' => $barang[$i]['produk_id'],
                        'transaksi_id' => $id->id,
                    ],
                ]);
            }
        }
    }
}
