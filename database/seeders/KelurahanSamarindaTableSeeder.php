<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class KelurahanSamarindaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data_kelurahan = [
            'Rawa Makmur',
            'Handil Bakti',
            'Bukuan',
            'Simpang Pasir',
            'Bantuas',
            'Sungai Keledang',
            'Baqa',
            'Mesjid',
            'Mangkupalas',
            'Tenun',
            'Gunung Panjang',
            'Teluk Lerong Ilir',
            'Jawa',
            'Air Putih',
            'Sidodadi',
            'Air Hitam',
            'Dadi Mulya',
            'Gunung Kelua',
            'Bukit Pinang',
            'Selili',
            'Sungai Dama',
            'Sidomulyo',
            'Sidodamai',
            'Pelita',
            'Sempaja Selatan',
            'Lempake',
            'Sungai Siring',
            'Sempaja Utara',
            'Tanah Merah',
            'Sempaja Barat',
            'Sempaja Timur',
            'Budaya Pampang',
            'Loa Bakung',
            'Loa Buah',
            'Karang Asam Ulu',
            'Lok Bahu',
            'Teluk Lerong Ulu',
            'Karang Asam Ilir',
            'Karang Anyar',
            'Sungai Kapih',
            'Sambutan',
            'Makroman',
            'Sindang Sari',
            'Pulau Atas',
            'Temindung Permai',
            'Sungai Pinang Dalam',
            'Gunung Lingai',
            'Mugirejo',
            'Bandara',
            'Karang Mumus',
            'Pelabuhan',
            'Pasar Pagi',
            'Bugis',
            'Sungai Pinang Luar',
            'Simpang Tiga',
            'Tani Aman',
            'Sengkotek',
            'Harapan Baru',
            'Rapak Dalam'
        ];

        foreach ($data_kelurahan as $key => $value) {
            DB::table('kelurahan')->insert([
                [
                    'nama' => $value,
                    'ongkir' => 0,
                    'kabupaten_id' => 1,
                ],
            ]);
        }
    }
}
