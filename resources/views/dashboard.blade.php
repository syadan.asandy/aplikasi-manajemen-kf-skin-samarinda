@extends('layouts.template')
@section('title', 'Dashboard | Admin')
@section('contentHeader', 'Dashboard')
@section('breadCrumb')
    <li class="breadcrumb-item active">Dashboard</li>
@endsection

@section('content')
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-4 col-6">
          <!-- small box -->
          <div class="small-box bg-info">
            <div class="inner">
              <h3>{{ $totalProduk }}</h3>

              <p>Produk</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="{{ route('produk.index') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-6">
          <!-- small box -->
          <div class="small-box bg-success">
            <div class="inner">
              <h3>{{ $totalTransaksi }}</h3>

              <p>Transaksi</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="{{ URL::current() }}/../transaksi" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-6">
          <!-- small box -->
          <div class="small-box bg-warning">
            <div class="inner">
              <h3>{{ $totalCustomer }}</h3>

              <p>Customer</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="{{ URL::current() }}/../customer" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        {{-- <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-danger">
            <div class="inner">
              <h3>65</h3>

              <p>Unique Visitors</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col --> --}}
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-7 connectedSortable">
          <!-- Custom tabs (Charts with tabs)-->
          <!-- BAR CHART -->
          <div class="card card-success">
            <div class="card-header">
              <h3 class="card-title">Grafik Penjualan per bulan</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                </button>
                <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
              </div>
            </div>
            <div class="card-body">
              <div class="chart">
                <canvas id="barChart" style="min-height: 250px; height: 450px; max-height: 450px; max-width: 100%;"></canvas>
              </div>
            </div>
            <!-- /.card-body -->
          </div>
        </section>
        <section class="col-lg-5 connectedSortable">

          <!-- /.card -->
          <!-- DONUT CHART -->
          <div class="card card-danger">
            <div class="card-header">
              <h3 class="card-title">Grafik Keseluruhan Produk Terjual</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                </button>
                <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
              </div>
            </div>
            <div class="card-body">
              <canvas id="donutChart" style="min-height: 450px; height: 450px; max-height: 450px; max-width: 100%;"></canvas>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->

        {{-- </section> --}}
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        {{-- <section class="col-lg-5 connectedSortable"> --}}
          {{-- <!-- Calendar -->
          <div class="card bg-gradient-success">
            <div class="card-header border-0">

              <h3 class="card-title">
                <i class="far fa-calendar-alt"></i>
                Calendar
              </h3>
              <!-- tools card -->
              <div class="card-tools">
                <!-- button with a dropdown -->
                <div class="btn-group">
                  <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown" data-offset="-52">
                    <i class="fas fa-bars"></i></button>
                  <div class="dropdown-menu" role="menu">
                    <a href="#" class="dropdown-item">Add new event</a>
                    <a href="#" class="dropdown-item">Clear events</a>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item">View calendar</a>
                  </div>
                </div>
                <button type="button" class="btn btn-success btn-sm" data-card-widget="collapse">
                  <i class="fas fa-minus"></i>
                </button>
                <button type="button" class="btn btn-success btn-sm" data-card-widget="remove">
                  <i class="fas fa-times"></i>
                </button>
              </div>
              <!-- /. tools -->
            </div>
            <!-- /.card-header -->
            <div class="card-body pt-0">
              <!--The calendar -->
              <div id="calendar" style="width: 100%"></div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card --> --}}
        </section>
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
@endsection

@section('script')
<script>
    $(function () {
    'use strict'

      /* ChartJS
       * -------
       * Here we will create a few charts using ChartJS
       */
       let label = new Array();
       let data = new Array();

        <?php foreach (array_reverse($transaksiRange) as $key => $value) { ?>
            label.push(<?= '\''.$value.'\'' ?>);
        <?php } ?>

        <?php foreach (array_reverse($transaksiVal) as $key => $value) { ?>
            data.push(<?= '\''.$value.'\'' ?>);
        <?php } ?>

        var areaChartData = {
            labels  : label,
            datasets: [
                {
                label               : 'Transaksi',
                backgroundColor     : 'rgba(60,141,188,0.9)',
                borderColor         : 'rgba(60,141,188,0.8)',
                pointRadius          : false,
                pointColor          : '#3b8bba',
                pointStrokeColor    : 'rgba(60,141,188,1)',
                pointHighlightFill  : '#fff',
                pointHighlightStroke: 'rgba(60,141,188,1)',
                data                : data
                },
            ]
        }

      //-------------
      //- BAR CHART -
      //-------------
      var barChartCanvas = $('#barChart').get(0).getContext('2d')
      var barChartData = jQuery.extend(true, {}, areaChartData)
      var temp0 = areaChartData.datasets[0]
      barChartData.datasets[0] = temp0

      var barChartOptions = {
        responsive              : true,
        maintainAspectRatio     : false,
        datasetFill             : false,
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true,
                    suggestedMax:10,
                }
            }]
        }
      }

      var barChart = new Chart(barChartCanvas, {
        type: 'bar',
        data: barChartData,
        options: barChartOptions
      })

//-------------
    //- DONUT CHART -
    //-------------
    // Get context with jQuery - using jQuery's .get() method.
        let labelDonut = new Array();
        let dataDonut = new Array();

        <?php foreach ($produkLabel as $key => $value) { ?>
            labelDonut.push(<?= '\''.$value.'\'' ?>);
        <?php } ?>

        <?php foreach ($produkData as $key => $value) { ?>
            @if(isset($value))
                dataDonut.push(<?= '\''.$value.'\'' ?>);
            @else
                dataDonut.push(0);
            @endif
        <?php } ?>

        var donutChartCanvas = $('#donutChart').get(0).getContext('2d')
        var donutData        = {
            labels: labelDonut,
            datasets: [
                {
                    data: dataDonut,
                    backgroundColor : ['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc', '#d2d6de', '#AED581', '#78909C', '#26A69A', '#7E57C2', '#EC407A', '#B71C1C', '#827717', '#0277BD', '#3E2723', '#880E4F'],
                }
            ]
        }
        var donutOptions     = {
            maintainAspectRatio : false,
            responsive : true,
        }
        //Create pie or douhnut chart
        // You can switch between pie and douhnut using the method below.
        var donutChart = new Chart(donutChartCanvas, {
            type: 'doughnut',
            data: donutData,
            options: donutOptions
        })

        // var calendar = new Calendar(document.getElementById('calendar'), {
        //     plugins: [ 'bootstrap', 'interaction', 'dayGrid', 'timeGrid' ],
        //     header    : {
        //         left  : 'prev,next today',
        //         center: 'title',
        //         right : 'dayGridMonth,timeGridWeek,timeGridDay'
        //     },
        //     'themeSystem': 'bootstrap',
        //     editable  : true,
        //     droppable : true, // this allows things to be dropped onto the calendar !!!
        //     drop      : function(info) {
        //         // is the "remove after drop" checkbox checked?
        //         if (checkbox.checked) {
        //         // if so, remove the element from the "Draggable Events" list
        //         info.draggedEl.parentNode.removeChild(info.draggedEl);
        //         }
        //     }
        //     });

        // calendar.render();
    })
  </script>
@endsection
