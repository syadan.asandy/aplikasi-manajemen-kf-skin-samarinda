<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Login KF Skin Samarinda</title>
    <link rel = "icon" href =
        "{{ asset('public/img/profil/logo.png') }}"
        type = "image/x-icon"
    />
    <link
      href="https://fonts.googleapis.com/css?family=Karla:400,700&display=swap"
      rel="stylesheet"
    />
    <link
      rel="stylesheet"
      href="{{ asset('public/css/materialdesignicons.min.css') }}"
    />
    <link
      rel="stylesheet"
      href="{{ asset('public/css/bootstrap.min.css') }}"
    />
    <link rel="stylesheet" href="{{ asset('public/css/login.css') }}" />

    <style>
        @media screen and (max-width: 770px) {
            body {
                background-size: cover;
                background-image: url({{ asset('public/img/kebutuhan/bg-login.jpeg') }});
            }

            .card {
                text-align: center;

            }

            form {
                margin: auto;
            }
        }
    </style>
  </head>
  <body>
    <main class="d-flex align-items-center min-vh-100 py-3 py-md-0">
      <div class="container" style="margin-top: 10px; margin-bottom: 10px">
        <div class="card login-card">
          <div class="row no-gutters">
            <div class="col-md-5">
              <img
                src="{{ asset('public/img/kebutuhan/bg-login.jpeg') }}"
                alt="login"
                class="login-card-img"
              />
            </div>
            <div class="col-md-7">
              <div class="card-body">
                <div class="brand-wrapper">
                  <img src="{{ asset('public/img/profil/logo.png') }}" alt="logo" class="logo" style="width: 200px; height: 60px;" />
                </div>
                <p class="login-card-description">Sign into your account</p>
                <form action="{{ route('login') }}" method="POST">
                    @csrf
                  <div class="form-group">
                    <label for="email" class="sr-only">Email</label>
                    <input
                      type="email"
                      name="email"
                      id="email"
                      class="form-control"
                      placeholder="Email address"
                    />
                  </div>
                  <div class="form-group mb-4">
                    <label for="password" class="sr-only">Password</label>
                    <input
                      type="password"
                      name="password"
                      id="password"
                      class="form-control"
                      placeholder="***********"
                    />
                  </div>
                  <button class="btn btn-block login-btn mb-4" type="submit">MASUK</button>

                </form>
                <br>
                <br>
                <br>
                <br>
                <br>
                {{-- <a href="#!" class="forgot-password-link">Forgot password?</a>
                <p class="login-card-footer-text">
                  Don't have an account?
                  <a href="#!" class="text-reset">Register here</a>
                </p> --}}
                <nav class="login-card-footer-nav">
                  <a href="#!">Terms of use.</a>
                  <a href="#!">Privacy policy</a>
                </nav>
              </div>
            </div>
          </div>
        </div>
        <div class="text-sm" style="font-size: 10pt; text-align: center;">
            Developed by Syadan Asandy Nugraha.  &copy; 2020
        </div>
      </div>
    </main>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
  </body>
</html>
