@extends('layouts.template')
@section('title', 'Ubah Kelurahan | Admin')
@section('contentHeader', 'Ubah Kelurahan')
@section('breadCrumb')
    <li class="breadcrumb-item"><a href="{{ route('kelurahan.index') }}">Data Kelurahan</a></li>
    <li class="breadcrumb-item active">Ubah Data</li>
@endsection

@section('content')
<!-- Main content -->
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-12 bg-gray disabled rounded">
            <div class="card-body">
                {!! Form::model($data, ['method' => 'PATCH', 'action' => ['App\Http\Controllers\KelurahanController@update',$data->nama], 'id' => 'kelas_create_form']) !!}
                {{-- @csrf --}}
                @include('kelurahan.form')
            </div>
        </div>
      </div>
      <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
  <!-- /.content -->
@endsection
