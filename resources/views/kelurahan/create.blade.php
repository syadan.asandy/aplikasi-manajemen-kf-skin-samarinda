@extends('layouts.template')
@section('title', 'Tambah Kelurahan | Admin')
@section('contentHeader', 'Tambah Kelurahan')
@section('breadCrumb')
    <li class="breadcrumb-item"><a href="{{ route('kelurahan.index') }}">Data Kelurahan</a></li>
    <li class="breadcrumb-item active">Tambah Data</li>
@endsection

@section('content')
<!-- Main content -->
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-12 bg-gray disabled rounded">
            <div class="card-body">
                {!! Form::open(['method' => 'POST', 'action' => 'App\Http\Controllers\KelurahanController@store', 'id' => 'kelas_create_form']) !!}
                @csrf
                @include('kelurahan.form')
            </div>
        </div>
      </div>
      <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
  <!-- /.content -->
@endsection
