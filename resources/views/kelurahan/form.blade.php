<div class="form-group">
    {!! Form::label('nama', 'Nama: ') !!}
    {!! Form::text('nama', null, ['class' => 'form-control', 'required']) !!}
    @if ($errors->has('nama'))
        <span class="text-danger">{{ $errors->first('nama') }}</span>
    @endif
</div>
<div class="form-group">
    {!! Form::label('kabupaten_id', 'Kabupaten: ') !!}
    <select name="kabupaten_id" id="" class="form-control">
        @foreach ($kabupaten as $key => $item)
            @if ($item->id == $data->kabupaten_id)
                <option value="{{ $item->id }}" selected>{{ $item->nama }}</option>
            @else
                <option value="{{ $item->id }}">{{ $item->nama }}</option>
            @endif
        @endforeach
    </select>
    @if ($errors->has('kabupaten_id'))
        <span class="text-danger">{{ $errors->first('kabupaten_id') }}</span>
    @endif
</div>
{{-- <div class="form-group">
    {!! Form::label('foto', 'Foto: ') !!} <br>
    {!! Form::file('foto', null, ['class' => 'form-control']) !!}
</div> --}}
{{-- {!! Form::hidden('user_id', Auth::user()->id, ['class' => 'form-control']) !!} --}}
{!! Form::submit($submitbuttontext, ['class' => 'btn btn-md btn-success']) !!}
{!! Form::close() !!}
