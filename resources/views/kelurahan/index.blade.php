@extends('layouts.template')
@section('title', 'Data Kelurahan | Admin')
@section('contentHeader', 'Data Kelurahan')
@section('breadCrumb')
    <li class="breadcrumb-item active">Data Kelurahan</li>
@endsection

@section('content')
{{-- content --}}
    <div class="container-fluid bg-secondary rounded">
    <!-- Small boxes (Stat box) -->
      <div class="row pt-4">
          <div class="col-lg-6">
            <a href="{{ route('kelurahan.create') }}" class="btn btn-md btn-primary">Tambah Kelurahan</a>
          </div>
      </div>
      <div class="row mt-3 p-2">
        <div class="col-lg-12 p-4 bg-light rounded">
            <table id="tableData" class="table table-striped table-hover table-bordered dt-responsive nowrap" style="width:100%">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Kelurahan</th>
                        <th>Kabupaten</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $index => $each)
                        <tr>
                            <td>{{ $index+1 }}</td>
                            <td>{{ $each->nama }}</td>
                            <td>{{ $each->kabupaten->nama }}</td>
                            <td>
                                <a href="{{ route('kelurahan.edit', $each->nama) }}" class="btn btn-sm btn-warning">
                                    <i class="fas fa-pen"></i>
                                </a>
                                <form action="{{ route('kelurahan.destroy', $each->nama) }}" method="post" style="display:inline">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-sm btn-danger btn-delete">
                                        <i class="fas fa-trash"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
      </div>
    </div><!-- /.container-fluid -->
{{-- endcontent --}}
@endsection
