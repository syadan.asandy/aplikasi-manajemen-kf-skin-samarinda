@extends('layouts.template')
@section('title', 'Ubah Kabupaten | Admin')
@section('contentHeader', 'Ubah Kabupaten')
@section('breadCrumb')
    <li class="breadcrumb-item"><a href="{{ route('kabupaten.index') }}">Data Kabupaten</a></li>
    <li class="breadcrumb-item active">Ubah Data</li>
@endsection

@section('content')
<!-- Main content -->
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-12 bg-gray disabled rounded">
            <div class="card-body">
                {!! Form::model($data, ['method' => 'PATCH', 'action' => ['App\Http\Controllers\KabupatenController@update',$data->id], 'id' => 'kelas_create_form']) !!}
                {{-- @csrf --}}
                @include('kabupaten.form')
            </div>
        </div>
      </div>
      <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
  <!-- /.content -->
@endsection
