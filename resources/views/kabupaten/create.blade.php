@extends('layouts.template')
@section('title', 'Tambah Kabupaten | Admin')
@section('contentHeader', 'Tambah Kabupaten')
@section('breadCrumb')
    <li class="breadcrumb-item"><a href="{{ route('produk.index') }}">Data Kabupaten</a></li>
    <li class="breadcrumb-item active">Tambah Data</li>
@endsection

@section('content')
<!-- Main content -->
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-12 bg-gray disabled rounded">
            <div class="card-body">
                {!! Form::open(['method' => 'POST', 'action' => 'App\Http\Controllers\KabupatenController@store', 'id' => 'kelas_create_form']) !!}
                @csrf
                @include('kabupaten.form')
            </div>
        </div>
      </div>
      <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
  <!-- /.content -->
@endsection
