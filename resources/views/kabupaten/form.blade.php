<div class="form-group">
    {!! Form::label('nama', 'Nama: ') !!}
    {!! Form::text('nama', null, ['class' => 'form-control', 'required']) !!}
    @if ($errors->has('nama'))
        <span class="text-danger">{{ $errors->first('nama') }}</span>
    @endif
</div>
{!! Form::submit($submitbuttontext, ['class' => 'btn btn-md btn-success']) !!}
{!! Form::close() !!}
