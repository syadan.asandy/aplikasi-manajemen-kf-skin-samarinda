@extends('layouts.template')
@section('title', 'Data Transaksi | Admin')
@section('contentHeader', 'Data Transaksi')
@section('breadCrumb')
    <li class="breadcrumb-item active">Data Transaksi</li>
@endsection

@section('content')
{{-- content --}}
<div class="container-fluid bg-secondary rounded">
    <!-- Small boxes (Stat box) -->
      <div class="row pt-4">
          <div class="col-lg-6">
            <a href="{{ route('transaksi.create') }}" class="btn btn-md btn-primary">Tambah Transaksi</a>
          </div>
      </div>
      <div class="row mt-3 p-2">
        <div class="col-lg-12 p-4 bg-light rounded">
            <table id="tableData" class="table table-striped table-hover table-bordered dt-responsive nowrap" style="width:100%">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nomor Transaksi</th>
                        <th>Customer</th>
                        <th>Total harga</th>
                        <th>Admin</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $index => $each)
                        @php
                            $total = 0
                        @endphp
                        <tr>
                            <td>{{ $index+1 }}</td>
                            <td>{{ $each->nomor }}</td>
                            <td>
                                @if (isset($each->customer->nama))
                                    {{ $each->customer->nama }}
                                @else
                                    {{ $each->customer()->withTrashed()->first()->nama }}
                                @endif
                            </td>
                            <td>Rp. {{ number_format($each->total, 0, ',', '.') }} </td>
                            <td>{{ $each->users->name }}</td>
                            <td>
                                <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#modal{{$index+1}}">
                                    <i class="fas fa-info-circle"></i>
                                </button>
                                <a href="{{ route('transaksi.cetak', $each->nomor) }}" class="btn btn-sm btn-danger"><i class="fas fa-print"></i></a>
                            </td>
                        </tr>
                        {{-- modal --}}
                        <div class="modal fade" id="modal{{ $index+1 }}" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="staticBackdropLabel">Rincian Transaksi</h5>
                                    </div>
                                    <div class="modal-body">
                                        {{-- @yield('rinci') --}}
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="col-md-8 text-sm">
                                                    <div class="row">
                                                        <div class="col">KF Skin</div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col">{{ $each->users->name }}</div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col">Sambutan, Samarinda</div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 text-sm">
                                                    <div class="row">
                                                        <div class="col">Samarinda, {{ date('j F Y', strtotime($each->created_at)) }}</div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col">Kepada,
                                                            @if (isset($each->customer->nama))
                                                                {{ $each->customer->nama }}
                                                            @else
                                                                {{ $each->customer()->withTrashed()->first()->nama }}
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col">
                                                            @if (isset($each->customer->telepon))
                                                                {{ $each->customer->telepon }}
                                                            @else
                                                                {{ $each->customer()->withTrashed()->first()->telepon }}
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col">
                                                            @if (isset($each->customer->alamat))
                                                                {{ $each->customer->alamat }}, {{ $each->customer->kelurahan->nama }}
                                                            @else
                                                                {{ $each->customer()->withTrashed()->first()->alamat }}, {{ $each->customer()->withTrashed()->first()->kelurahan->nama }}
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row mt-4">
                                                <div class="col">
                                                    <div class="row">
                                                        <div class="col-sm-2">Qty</div>
                                                        <div class="col-sm-6">Nama Produk</div>
                                                        <div class="col-sm-2">Harga Satuan</div>
                                                        <div class="col-sm-2">Jumlah</div>
                                                    </div>
                                                    @foreach ($each->rinci as $rinci)
                                                        @php
                                                            $total += $rinci->harga*$rinci->qty
                                                        @endphp
                                                        <div class="row text-sm">
                                                            <div class="col-sm-2">{{ $rinci->qty }}</div>
                                                            <div class="col-sm-6">
                                                                @if (isset($rinci->produk->nama))
                                                                    {{ $rinci->produk->nama }}
                                                                @else
                                                                    {{ $rinci->produk()->withTrashed()->first()->nama }}
                                                                @endif
                                                            </div>
                                                            <div class="col-sm-2">Rp. {{ $rinci->harga }}</div>
                                                            <div class="col-sm-2">Rp. {{ $rinci->harga*$rinci->qty }}</div>
                                                        </div>
                                                    @endforeach
                                                    <div class="row">
                                                        <div class="col-sm-2"></div>
                                                        <div class="col-sm-6"></div>
                                                        <div class="col-sm-2"></div>
                                                        <div class="border-top" style="width: 100px; padding-left: 15px;">
                                                            <div class="row">
                                                                <div class="col text-sm">
                                                                    <b>Rp. {{ $total }}</b>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                        <button type="button" class="btn btn-primary">Cetak?</button>

                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </tbody>
            </table>
        </div>
      </div>
    </div><!-- /.container-fluid -->
{{-- endcontent --}}
@endsection
{{--
@section('script')
    <script>
        function cetak(params) {
            const Url = '/cetak-nota';
            // alert(Object.values());
            $.post(Url, params, function(params, status){
                console.log(`${params} and status is ${status}`)
            });
        }
    </script>
@endsection --}}
