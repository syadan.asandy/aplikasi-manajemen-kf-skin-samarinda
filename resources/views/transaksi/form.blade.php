<div class="form-group">
    {!! Form::label('customer', 'Customer: ') !!}
    <select name="customer" id="customer" class="form-control">
        @foreach ($customer as $each)
            <option value="{{ $each->id }}" onclick="setHarga({{ $each->kelurahan->ongkir }})">{{ $each->nama }}</option>
        @endforeach
    </select>
    {{-- {!! Form::select('customer', $customer => $each, null, ['class' => 'form-control', 'id' => 'customer', 'required', 'onchange="setHarga()"']) !!} --}}
    @if ($errors->has('customer'))
        <span class="text-danger">{{ $errors->first('nama') }}</span>
    @endif
</div>

<div class="row">
    <div class="col-md-7">
        <div class="form-group">
            <label for="produk">Produk:</label>
            <select name="produk[]" id="optProduk" class="form-control">
                @foreach ($produk as $each)
                    <option value="{{ $each->id }}">{{ $each->nama }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label for="diskon">Diskon:</label>
            {!! Form::text('diskon[]', null, ['class' => 'form-control', 'required', 'id' => 'diskon', 'placeholder' => 'diskon']) !!}
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            {!! Form::label('qty', 'Qty: ') !!}
            {!! Form::text('qty[]', null, ['class' => 'form-control', 'required', 'id' => 'qty', 'placeholder' => 'qty']) !!}
            @if ($errors->has('qty'))
                <span class="text-danger">{{ $errors->first('qty') }}</span>
            @endif
        </div>
    </div>
</div>
{{-- <div class="form-group">
    {!! Form::label('produk', 'Produk: ') !!}
    {!! Form::select('', $produk, null, ['class' => 'form-control', 'required', 'id' => 'optProduk']) !!}
    @if ($errors->has('produk'))
        <span class="text-danger">{{ $errors->first('produk') }}</span>
    @endif
</div> --}}

<div class="addElemen">
    <div class="col-md-12">
        <div class="form-group">
            <label for="titlePT" id="titlePT" style="display: none">Produk Tambahan:</label>
        </div>
    </div>
</div>

<div class="form-group">
    {!! Form::label('ongkir', 'Ongkir: ') !!}
    {{-- {!! Form::radio('ongkir_status', ['Otomatis', 'Pilihan', 'Custom'], null, ['class' => 'form-control']) !!} --}}
    {!! Form::select('ongkir', ['Otomatis', 'Custom'], null, ['class' => 'form-control', 'id' => 'ongkir', 'required', 'onclick="customHarga()"']) !!}
    @if ($errors->has('ongkir'))
        <span class="text-danger">{{ $errors->first('ongkir') }}</span>
    @endif
</div>

<div class="form-group">
    {!! Form::label('harga', 'Harga: ') !!}
    {{-- {!! Form::radio('ongkir_status', ['Otomatis', 'Pilihan', 'Custom'], null, ['class' => 'form-control']) !!} --}}
    {{-- {!! Form::text($name, $value, [$options]) !!} --}}
    {!! Form::text('harga', null, ['class' => 'form-control', 'id' => 'harga', 'required']) !!}
    @if ($errors->has('harga'))
        <span class="text-danger">{{ $errors->first('harga') }}</span>
    @endif
</div>

<button type="button" class="btn btn-md btn-info" id="tambahData">Tambah Produk</button>
{{-- <a href="#" class="btn btn-sm btn-info" id="tambahData">Tambah Produk</a> --}}
{!! Form::hidden('user_id', Auth::user()->id, ['class' => 'form-control']) !!}
{!! Form::submit($submitbuttontext, ['class' => 'btn btn-md btn-success']) !!}
{!! Form::close() !!}
