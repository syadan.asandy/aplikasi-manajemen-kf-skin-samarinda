{{-- <!-- Main Sidebar Container --> sidebar-dark-primary --}}
<aside class="main-sidebar sidebar-dark elevation-4 bg-white">
    <!-- Brand Logo -->
    <a href="{{route('dashboard')}}" class="brand-link">
      <img src="{{ asset('public/img/users/'.Auth::user()->logo_web) }}" alt="AdminLTE Logo" class="brand-image" style="width: 80%;">
      <br>
      {{-- <span class="brand-text font-weight-light">KF Skin</span> --}}
    </a>

    <!-- Sidebar -->
    <div class="sidebar scrolling">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{ asset('public/img/users/'.Auth::user()->profile_photo_path) }}" class="img-circle elevation-2" alt="User Image">
          {{-- <i class="fa fas fa-women"></i> --}}
        </div>
        <div class="info">
          <a href="{{ route('konfigurasi.index') }}" class="d-block text-secondary">{{ Auth::user()->name }}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column test" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-header">MENU</li>
          <li class="nav-item">
            <a href="{{ route('produk.index') }}" class="nav-link">
              <i class="nav-icon ion ion-bag"></i>
              <p>
                Produk
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('customer.index') }}" class="nav-link">
              <i class="nav-icon ion ion-person"></i>
              <p>
                Customer
              </p>
            </a>
          </li>
          {{-- <li class="nav-item">
            <a href="{{ route('transaksi.index') }}" class="nav-link">
              <i class="nav-icon ion ion-stats-bars"></i>
              <p>
                Transaksi
              </p>
            </a>
          </li> --}}
          <li class="nav-item listCollapse">
            <a href="#transaksiCollapse" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="collapseExample" class="nav-link">
              <i class="nav-icon ion ion-stats-bars"></i>
              <p>
                Transaksi
              </p>
              <i class="fas fa-sort-down"></i>
            </a>
            <ul id="transaksiCollapse" class="collapse nav nav-pills ml-4" >
                <li class="nav-item">
                    <a href="{{ route('transaksi.create') }}" class="nav-link">
                        <i class="nav-icon fa fa-shopping-cart"></i>
                        <p>
                          Tambah
                        </p>
                      </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('transaksi.index') }}"  class="nav-link">
                        <i class="nav-icon fa fa-database"></i>
                        <p>
                          Data
                        </p>
                      </a>
                </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="{{ route('ongkir.index') }}" class="nav-link">
              <i class="nav-icon fa fa-taxi"></i>
              <p>
                Ongkir
              </p>
            </a>
          </li>
          <li class="nav-item listCollapse">
            <a href="#wilayahCollapse" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="collapseExample" class="nav-link">
              <i class="nav-icon fa fa-globe"></i>
              <p>
                Wilayah
              </p>
              <i class="fas fa-sort-down"></i>
            </a>
            <ul id="wilayahCollapse" class="collapse nav nav-pills ml-4" >
                <li class="nav-item">
                    <a href="{{ route('kabupaten.index') }}" class="nav-link">
                        <i class="nav-icon fa fa-building"></i>
                        <p>
                          Kabupaten
                        </p>
                      </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('kelurahan.index') }}"  class="nav-link">
                        <i class="nav-icon fa fa-home"></i>
                        <p>
                          Kelurahan
                        </p>
                      </a>
                </li>
            </ul>
          </li>
          <li class="nav-item listCollapse">
            <a href="#laporanCollapse" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="collapseExample" class="nav-link">
              <i class="nav-icon fa fa-book"></i>
              <p>
                Laporan
              </p>
              <i class="fas fa-sort-down"></i>
            </a>
            <ul id="laporanCollapse" class="collapse nav nav-pills ml-4" >
                <li class="nav-item">
                    <a href="{{ route('laporan.index', ['laporan' => 'bulanan']) }}" class="nav-link">
                        <i class="nav-icon fa fa-calendar-day"></i>
                        <p>
                          Bulanan
                        </p>
                      </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('laporan.index', ['laporan' => 'tahunan']) }}"  class="nav-link">
                        <i class="nav-icon fa fa-calendar-week"></i>
                        <p>
                          Tahunan
                        </p>
                      </a>
                </li>
            </ul>
          </li>
          <li class="nav-item listCollapse">
            <a href="#sampahCollapse" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="collapseExample" class="nav-link">
              <i class="nav-icon fa fa-trash"></i>
              <p>
                Sampah
              </p>
              <i class="fas fa-sort-down"></i>
            </a>
            <ul id="sampahCollapse" class="collapse nav nav-pills ml-4" >
                <li class="nav-item">
                    <a href="{{ route('sampah', ['jenis' => 'produk']) }}" class="nav-link">
                        <i class="nav-icon ion ion-bag"></i>
                        <p>
                          Produk
                        </p>
                      </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('sampah', ['jenis' => 'customer']) }}"  class="nav-link">
                        <i class="nav-icon ion ion-person"></i>
                        <p>
                          Customer
                        </p>
                      </a>
                </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="{{ route('konfigurasi.index') }}" class="nav-link">
              <i class="nav-icon fa fa-wrench"></i>
              <p>
                Konfigurasi
              </p>
            </a>
          </li>
          <li class="nav-item logout" style=" cursor: pointer; position: fixed; bottom: 10px;">
                <form method="POST" action="{{ route('logout') }}">
                    @csrf
                    <button type="submit" class="btn nav-link">
                        <i class="nav-icon ion ion-log-out"></i>
                        <p>
                            {{ __('Logout') }}
                        </p>
                    </button>
                </form>
            </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <script>

    const listCollapse = document.querySelectorAll(".listCollapse");

    const link = document.querySelectorAll(".listCollapse a");

    listCollapse.forEach((elm,idx) => {

        elm.addEventListener("click",()=>{

            // link.forEach(elem => {

                for(let i = 0; i < link.length - 1 ; i++) {
                    if(link[i].classList.contains("collapsed")){
                        console.log("Ini uda collapsed");
                        break;
                    }
                    else{
                        console.log("Gagal Collapsed");
                    }
                }

            // })
        })
    })

//   const scroll = document.querySelector(".test");
//     const logoutButton = document.querySelector(".logout");
//     const list = document.querySelectorAll(".test li");

// setInterval(()=>{
//     console.log(`${scroll} + ${scroll.clientHeight}`);
// },1000)


    // list.forEach((elm,idx) => {
    //     elm.addEventListener("click",()=>{
    //     if(scroll.clientHeight > 471) {
    //         console.log("Udah disini");
    //         logoutButton.style.background = "black";
    //     }
    //     else{
    //         console.log("udah out");
    //         logoutButton.style.background = "none";
    //     }
    //     })
    // })


    // list.addEventListener("click",() =>{
    //     if(scroll.clientHeight > 100) {
    //     console.log("Udahndisini");
    //     logoutButton.style.position = "none";
    // }
    // })

//    const testFunction = () => {

//     if(scroll.clientHeight > 100) {
//         console.log("Udahndisini");
//         logoutButton.style.position = "none";
//     }
//    }


  </script>
