@include('layouts.header')
@include('layouts.navbar')
@include('layouts.sidebar')
@include('layouts.contentHeader')

<!-- Main content -->
<section class="content ml-2">

        @yield('content')

</section>
<!-- /.content -->
@include('layouts.footer')
