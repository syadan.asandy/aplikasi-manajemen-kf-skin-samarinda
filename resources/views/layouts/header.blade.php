<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>
      @yield('title')
  </title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel = "icon" href =
    "{{ asset('public/img/profil/logo.png') }}"
    type = "image/x-icon"
    />
  {{-- DataTable CSS --}}
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.6/css/responsive.bootstrap4.min.css">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('public/AdminLTE/plugins/fontawesome-free/css/all.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('public/AdminLTE/dist/css/adminlte.min.css') }}">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- SweetAlert -->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    {{-- <link rel="stylesheet" href="{{ asset('public/css/bootstrap-select.css') }}">
    <link rel="stylesheet" href="{{ asset('public/css/bootstrap-select.min.css') }}">

    <script src="{{ asset('public/js/bootstrap-select.js') }}"></script>
    <script src="{{ asset('public/js/bootstrap-select.min.js') }}"></script> --}}
{{--
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/css/bootstrap-select.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/js/bootstrap-select.js"></script> --}}
    {{-- <link rel="stylesheet" href="{{ asset('public/css/selectpicker.manual.css') }}"> --}}
    {{-- <script src="{{ asset('public/js/selectpicker.manual.js') }}"></script> --}}
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">
    <div class="swalNotif">
        @if (Session::has('success'))
            <script>
                swal("Josss Gandosss", "{!! Session::get('success') !!}", "success",{
                    button: "OK",
                    icon: "success",
                });
            </script>
        @endif
        @if (Session::has('warning'))
            <script>
                swal("Josss Gandosss", "{!! Session::get('warning') !!}", "success",{
                    button: "OK",
                    icon: "warning",
                });
            </script>
        @endif
        @if (Session::has('info'))
            <script>
                swal("Josss Gandosss", "{!! Session::get('info') !!}", "success",{
                    button: "OK",
                    icon: "info",
                });
            </script>
        @endif
        @if (Session::has('danger'))
            <script>
                swal("Josss Gandosss", "{!! Session::get('danger') !!}", "success",{
                    button: "OK",
                    icon: "error",
                });
            </script>
        @endif
    </div>
