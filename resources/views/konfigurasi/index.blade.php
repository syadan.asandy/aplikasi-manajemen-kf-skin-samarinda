@extends('layouts.template')
@section('title', 'Data Profil | Admin')
@section('contentHeader', 'Data Profil')
@section('breadCrumb')
    <li class="breadcrumb-item active">Data Profil</li>
@endsection

<style>
/* .emp-profile{
    padding: 3%;
    margin-top: 3%;
    margin-bottom: 3%;
    border-radius: 0.5rem;
    background: #fff;
} */
.profile-img{
    text-align: center;
}
.profile-img img{
    width: 70%;
    /* height: 100%; */
}
.profile-img .file {
    position: relative;
    overflow: hidden;
    margin-top: -20%;
    width: 70%;
    border: none;
    border-radius: 0;
    font-size: 15px;
    background: #212529b8;
}
.profile-img .file input {
    position: absolute;
    opacity: 0;
    right: 0;
    top: 0;
}
.profile-head h5{
    color: #333;
}
.profile-head h6{
    color: #0062cc;
}
.profile-edit-btn{
    border: none;
    border-radius: 1.5rem;
    width: 70%;
    padding: 2%;
    font-weight: 600;
    color: #6c757d;
    cursor: pointer;
}
.proile-rating{
    font-size: 12px;
    color: #818182;
    margin-top: 5%;
}
.proile-rating span{
    color: #495057;
    font-size: 15px;
    font-weight: 600;
}
.profile-head .nav-tabs{
    margin-bottom:5%;
}
.profile-head .nav-tabs .nav-link{
    font-weight:600;
    border: none;
}
.profile-head .nav-tabs .nav-link.active{
    border: none;
    border-bottom:2px solid #0062cc;
}
.profile-work{
    padding: 14%;
    margin-top: -15%;
}
.profile-work p{
    font-size: 12px;
    color: #818182;
    font-weight: 600;
    margin-top: 10%;
}
.profile-work a{
    text-decoration: none;
    color: #495057;
    font-weight: 600;
    font-size: 14px;
}
.profile-work ul{
    list-style: none;
}
.profile-tab label{
    font-weight: 600;
}
.profile-tab p{
    font-weight: 600;
    color: #0062cc;
}
</style>

@section('content')
{{-- content --}}
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    @if($error == 'validation.min.string' || $error == 'validation.min' || $error == 'validation.min.numeric' || $error == 'validation.min' )
                        <li>Password anda kurang dari 8 karakter</li>
                    @endif

                    @if($error == 'validation.same' )
                        <li>Password baru dan konfirmasi tidak sama</li>
                    @endif
                @endforeach
            </ul>
        </div>
    @endif
    <div class="container-fluid bg-secondary rounded">
    <!-- Small boxes (Stat box) -->
      <div class="row mt-3 p-2">
        <div class="col-lg-12 p-4 bg-light rounded">
            {{-- <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
                <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
                <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
                <!------ Include the above in your HEAD tag ----------> --}}

                <div class="container emp-profile">
                            {{-- <form method="post"> --}}
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="profile-img">
                                            <img src="{{ asset('public/img/users/'.Auth::user()->profile_photo_path) }}" alt="" style="border-radius: 100%;"/>
                                            {{-- <div class="file btn btn-lg btn-primary">
                                                Change Photo
                                                <input type="file" name="photo"/>
                                            </div> --}}
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="profile-head" style="padding-top: 80px">
                                                    <h5>
                                                        {{ Auth::user()->name }}
                                                    </h5>
                                                    {{-- <h6>
                                                        Web Developer and Designer
                                                    </h6>
                                                    <p class="proile-rating">RANKINGS : <span>8/10</span></p> --}}
                                            <ul class="nav nav-tabs" id="myTab" role="tablist" style="margin-top: 30px;">
                                                <li class="nav-item">
                                                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Tentang</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Web</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" id="pass-tab" data-toggle="tab" href="#pass" role="tab" aria-controls="pass" aria-selected="false">Password</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    {{-- <div class="col-md-2">
                                        <input type="submit" class="profile-edit-btn" name="btnAddMore" value="Edit Profile"/>
                                    </div> --}}
                                </div>
                                <div class="row">
                                    <div class="col-md-4">

                                    </div>
                                    <div class="col-md-8">
                                        <div class="tab-content profile-tab" id="myTabContent">
                                            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                                <form method="POST" action="{{ route('konfigurasi.store') }}" enctype="multipart/form-data">
                                                    @method('POST')
                                                    @csrf
                                                        {{-- <div class="row">
                                                            <div class="col-md-6">
                                                                <label>User Id</label>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <p>Kshiti123</p>
                                                            </div>
                                                        </div> --}}
                                                        <input type="hidden" name="formstatus" value="about">
                                                        <input type="hidden" name="id" value="{{ Auth::user()->id }}">
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <label>Foto Profil</label>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <p>
                                                                    <input type="file" name="foto" id="foto" class="form-control" value="{{ asset('public/img/users/'.Auth::user()->profile_photo_path) }}">
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <label>Nama</label>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <p>
                                                                    <input class="form-control" type="text" name="nama" value="{{ Auth::user()->name }}" placeholder="Nama Lengkap">
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <label>Email</label>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <p>
                                                                    <input class="form-control" type="email" name="email" value="{{ Auth::user()->email }}" placeholder="example@example.com" readonly>
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <label>Telepon</label>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <p>
                                                                    <input class="form-control" type="text" name="telepon" id="" value="{{ Auth::user()->telepon }}" placeholder="Nomor Telepon" onkeypress="return angka(event)">
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <label>Kabupaten</label>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <p>
                                                                    <input class="form-control" type="text" name="kabupaten" id="" value="{{ Auth::user()->kabupaten }}" placeholder="Kabupaten">
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <label>Kecamatan</label>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <p>
                                                                    <input class="form-control" type="text" name="kecamatan" id="" value="{{ Auth::user()->kecamatan }}" placeholder="Kecamatan">
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <label>Alamat</label>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <p>
                                                                    <input class="form-control" type="text" name="alamat" id="" value="{{ Auth::user()->alamat }}" placeholder="Alamat">
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-8">

                                                            </div>
                                                            <div class="col-md-2">
                                                                <p>
                                                                    <button type="submit" class="btn btn-success">Simpan</button>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </form>
                                            </div>
                                            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab" >
                                                <form action="{{ route('konfigurasi.store') }}" method="POST" enctype="multipart/form-data">
                                                    @method('POST')
                                                    @csrf
                                                    <input type="hidden" name="formstatus" value="web">
                                                        <input type="hidden" name="id" value="{{ Auth::user()->id }}">
                                                        {{-- <div class="row">
                                                            <div class="col-md-6">
                                                                <label>User Id</label>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <p>Kshiti123</p>
                                                            </div>
                                                        </div> --}}
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <label>Logo Web</label>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <p>
                                                                    <input type="file" name="logo" id="" class="form-control">
                                                                    {{-- <input type="file" src="{{ asset('public/img/profil/logo.png') }}" alt=""> --}}
                                                                </p>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <p>
                                                                    <img src="{{ asset('public/img/users/'.Auth::user()->logo_web) }}" alt="Logo WEB" style="width: 100px;">
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="row mb-3">
                                                            <div class="col-md-4">
                                                                <label>Status Logo Nota</label>
                                                            </div>
                                                            <div class="col-md-4" class="form-control">
                                                                @if (Auth::user()->status_nota == 0)
                                                                    <div class="custom-control custom-radio custom-control-inline">
                                                                        <input type="radio" id="statusnotatext" name="statusnota" class="custom-control-input" value="0" checked="true">
                                                                        <label class="custom-control-label" for="statusnotatext">Text</label>
                                                                    </div>
                                                                    <div class="custom-control custom-radio custom-control-inline">
                                                                        <input type="radio" id="statusnotalogo" name="statusnota" class="custom-control-input" value="1">
                                                                        <label class="custom-control-label" for="statusnotalogo">Logo</label>
                                                                    </div>
                                                                @else
                                                                    <div class="custom-control custom-radio custom-control-inline">
                                                                        <input type="radio" id="statusnotatext" name="statusnota" class="custom-control-input" value="0">
                                                                        <label class="custom-control-label" for="statusnotatext">Text</label>
                                                                    </div>
                                                                    <div class="custom-control custom-radio custom-control-inline">
                                                                        <input type="radio" id="statusnotalogo" name="statusnota" class="custom-control-input" value="1" checked="true">
                                                                        <label class="custom-control-label" for="statusnotalogo">Logo</label>
                                                                    </div>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <label>Logo Nota</label>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <p>
                                                                    <input type="file" name="logonota" id="" class="form-control">
                                                                </p>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <p>
                                                                    <img src="{{ asset('public/img/users/'.Auth::user()->logo_nota) }}" alt="Logo Nota" style="width: 100px;">
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <label>Text Nota</label>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <p>
                                                                    <input class="form-control" type="text" name="text_nota" value="{{ Auth::user()->text_nota }}" placeholder="text">
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <label>Background Nota</label>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <p>
                                                                    <input type="file" name="bg_nota" id="" class="form-control">
                                                                </p>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <p>
                                                                    <img src="{{ asset('public/img/users/'.Auth::user()->bg_nota) }}" alt="Background Nota" style="width: 100px;">
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-8">

                                                            </div>
                                                            <div class="col-md-2">
                                                                <p>
                                                                    <button type="submit" class="btn btn-success">Simpan</button>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </form>
                                            </div>
                                            {{-- <div class="tab-pane fade" id="pass" role="tabpanel" aria-labelledby="pass-tab" >
                                                <form action="{{ route('password.update') }}" method="POST" enctype="multipart/form-data">
                                                    @method('POST')
                                                    @csrf
                                                    <input type="hidden" name="formstatus" value="password">
                                                        <input type="hidden" name="id" value="{{ Auth::user()->id }}">
                                                        <input type="hidden" name="token" value="{{ csrf_token() }}">

                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <label>Password Baru</label>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <p>
                                                                    <input class="form-control" type="password" name="password" placeholder="***********" required>
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <label>Konfirmasi Password Baru</label>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <p>
                                                                    <input class="form-control" type="password" name="password_confirmation" placeholder="***********" required>
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-8">

                                                            </div>
                                                            <div class="col-md-2">
                                                                <p>
                                                                    <button type="submit" class="btn btn-success">Simpan</button>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </form>
                                            </div> --}}
                                            <div class="tab-pane fade" id="pass" role="tabpanel" aria-labelledby="pass-tab" >
                                                <form action="{{ route('konfigurasi.store') }}" method="POST" enctype="multipart/form-data">
                                                    @method('POST')
                                                    @csrf
                                                    <input type="hidden" name="formstatus" value="password">
                                                        <input type="hidden" name="id" value="{{ Auth::user()->id }}">
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <label>Password Lama</label>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <p>
                                                                    <input class="form-control" type="password" name="oldPassword" placeholder="***********" required>
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <label>Password Baru</label>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <p>
                                                                    <input class="form-control" type="password" name="newPassword" placeholder="***********" required>
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <label>Konfirmasi Password Baru</label>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <p>
                                                                    <input class="form-control" type="password" name="confNewPassword" placeholder="***********" required>
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-8">

                                                            </div>
                                                            <div class="col-md-2">
                                                                <p>
                                                                    <button type="submit" class="btn btn-success">Simpan</button>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            {{-- </form> --}}
                        </div>
                    </div>
      </div>
    </div><!-- /.container-fluid -->
{{-- endcontent --}}
@endsection


@section('script')
    <script>

    </script>
@endsection
