@extends('layouts.template')
@section('title', 'Tambah Transaksi | Admin')
@section('contentHeader', 'Tambah Transaksi')
@section('breadCrumb')
    <li class="breadcrumb-item"><a href="{{ route('transaksi.index') }}">Data Transaksi</a></li>
    <li class="breadcrumb-item active">Tambah Data</li>
@endsection

@section('content')
<!-- Main content -->
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-12 bg-secondary rounded" style="padding: 20px 30px 20px 10px;">
            <div class="row">
                <div class="col-lg-6">
                    <div class="card-body">
                        {!! Form::open(['method' => 'POST', 'action' => 'App\Http\Controllers\TransaksiController@store']) !!}
                        @csrf
                        @include('transaksi.form')
                    </div>
                </div>
                <div class="col-lg-6 bg-light rounded pt-2 pl-3">
                    <table class="table table-sm">
                        {{-- <caption>List of users</caption> --}}
                        {{-- <thead>

                        </thead> --}}
                        <tbody>
                            <tr id='tr'></tr>
                            <tr>

                            </tr>
                        </tbody>
                      </table>
                </div>
            </div>
        </div>
      </div>
      <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
  <!-- /.content -->
@endsection

@section('script')
    <script>
        // $(document).ready(function() {
        //     $('#tambahData').click( function() {
        //         let table = document.getElementById('tr');
        //         let produk = document.getElementById('optProduk');
        //         let qty = document.getElementById('qty');
        //         let produkNama = "<td class='text text-muted text-sm'>"+ produk.options[produk.selectedIndex].text +"</td><td class='text text-sm text-bold'>x</td><td>"+ qty.value +"</td><td><button class='btn btn-sm btn-danger'><i class='fas fa-minus' onclick='rmElemen(elemen"+ produk.value +")'></i></button></td>";
        //         let produkValue = "<input id='elemen"+ produk.value +"' type=\"hidden\" name=\"produk[]\" value=\""+ produk.value +"\">";
        //         let qtyValue = "<input id='elemen"+ produk.value +"' type=\"hidden\" name=\"qty[]\" value=\""+ qty.value +"\">";
        //         $(table).before("<tr id='elemen"+ produk.value +"'>" + produkNama + "</tr>");
        //         $('#tambahData').before(produkValue);
        //         $('#tambahData').before(qtyValue);
        //         // i+=1;
        //     });
        // });
        $(document).ready(function() {
            $('#tambahData').click( function() {
                let rand = Math.random();
                let insert = '<div class="row" id="'+ rand +'">' +
                                '<div class="col-md-8">' +
                                    '<div class="form-group">' +
                                        // '<label for="produk">Produk:</label>' +
                                        '<select name="produk[]" id="optProduk" class="form-control">' +
                                            '@foreach ($produk as $each)' +
                                                '<option value="{{ $each->id }}">{{ $each->nama }}</option>' +
                                            '@endforeach' +
                                        '</select>' +
                                    '</div>' +
                                '</div>' +
                                '<div class="col-md-2">' +
                                    '<div class="form-group">' +
                                        // "{!! Form::label('qty', 'Qty: ') !!}" +
                                        '<input type="number" name="qty[]" class="form-control" required>' +
                                        // "{!! Form::number('qty[]', null, ['class' => 'form-control', 'required', 'id' => 'qty']) !!}" +
                                //         "@if ($errors->has('qty'))" +
                                //             "<span class=\"text-danger\">{{ $errors->first('qty') }}</span>" +
                                //         '@endif'+
                                    '</div>' +
                                '</div>' +
                                '<div class="col-md-2">' +
                                    "<a class='btn btn-md'><i style='border: solid 1px white; border-radius: 100%;' class='fas fa-minus-circle' onclick=\"rmElemen("+ rand +")\"></i></a>" +
                                '</div>' +

                            '</div>';

                $('.addElemen').before(insert);
            });
        });

        function rmElemen(a) {
            let b = document.getElementById(a);
            $(b).remove();
        }
    </script>
@endsection
