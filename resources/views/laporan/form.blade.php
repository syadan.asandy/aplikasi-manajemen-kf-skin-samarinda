@if ($_GET['laporan'] == 'bulanan')
    <div class="form-group">
        <label for="bulan">Bulan</label>
        <select name="bulan" id="" class="form-control">
            @foreach ($bulan as $each)
                <option value="{{ $each['angka'] }}">{{ $each['nama'] }}</option>
            @endforeach
        </select>
        @if ($errors->has('customer'))
            <span class="text-danger">{{ $errors->first('nama') }}</span>
        @endif
    </div>
@endif
<div class="form-group">
    <label for="tahun">Tahun</label>
    <select name="tahun" id="" class="form-control">
        @foreach ($tahun as $key => $each)
            <option value="{{ $key }}">{{ $key }}</option>
        @endforeach
    </select>
    @if ($errors->has('customer'))
        <span class="text-danger">{{ $errors->first('nama') }}</span>
    @endif
</div>
{!! Form::submit('Submit', ['class' => 'btn btn-md btn-success']) !!}
{!! Form::close() !!}
