@if (isset($angkabulan))
    @section('title', 'Laporan Bulanan | Admin')
    @section('contentHeader', 'Laporan Bulanan - '.$bulan.' '.$tahun)
@else
    @section('title', 'Laporan Tahunan | Admin')
    @section('contentHeader', 'Laporan Tahunan - '.$tahun)
@endif

@extends('layouts.template')
@section('breadCrumb')
    <li class="breadcrumb-item active">Data Laporan</li>
@endsection

@section('content')
{{-- content --}}
    <div class="container-fluid bg-secondary rounded">
    <!-- Small boxes (Stat box) -->
      <div class="row pt-4">
          <div class="col-lg-6">
            <form action="{{ route('laporan.update', $laporan) }}" method="post">
                @csrf
                @method('PUT')
                @if (isset($angkabulan))
                    {!! Form::hidden('bulan', $angkabulan, null) !!}
                @endif
                {!! Form::hidden('tahun', $tahun, null) !!}

                <button type="submit" class="btn btn-md btn-success">
                    <i class="fa fas fa-file-export">Unduh</i>
                </button>
            </form>
          </div>
      </div>
      <div class="row mt-3 p-2">
        <div class="col-lg-12 p-4 bg-light rounded">
            <table id="tableData" class="table table-striped table-hover table-bordered dt-responsive nowrap" style="width:100%">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nomor Transaksi</th>
                        <th>Customer</th>
                        <th>Total</th>
                        <th>Admin</th>
                        <th>Tanggal</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $index => $each)
                        <tr>
                            <td>{{ $index+1 }}</td>
                            <td>{{ $each->nomor }}</td>
                            <td>{{ isset($each->customer->nama) ? $each->customer->nama : $each->customer()->withTrashed()->first()->nama }}</td>
                            <td>Rp. {{ number_format($each->total, 0, ',', '.') }}</td>
                            <td>{{ $each->users->name }}</td>
                            <td>{{ date('d-m-Y', strtotime($each->created_at)) }}</td>
                            {{-- <td>
                                <a href="{{ route('customer.edit', $each->slug) }}" class="btn btn-sm btn-warning">
                                    <i class="fas fa-pen"></i>
                                </a>
                                <form action="{{ route('customer.destroy', $each->slug) }}" method="post" style="display:inline" onclick="return confirm('Dihapus?')">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-sm btn-danger">
                                        <i class="fas fa-trash"></i>
                                    </button>
                                </form>
                            </td> --}}
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
      </div>
    </div><!-- /.container-fluid -->
{{-- endcontent --}}
@endsection
