@php
    $total = 0;
    $tinggi = 0;
@endphp

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Nota {{ $data['customer']->nama . " | " . $data['nomor'] }}</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    {{-- <script type="text/javascript" src="http://livejs.com/live.js"></script> --}}

    <style>
        .headerKiri {
            width: 70%;
        }

        .headerKanan {
            width: 30%;
        }
        .pt {
            /* padding-top: 50px; */
            background-color: black;
        }
        .row {
            border: 2px solid black;
        }
    </style>
</head>
<body style="">
    {{-- <div style=" position: absolute; min-height: 100% !important; height: 100%; width: 100%; background-repeat: repeat; background-image: url('{{ asset('public/img/kebutuhan/keropi.jpg') }}');"> --}}
    <div style="border-radius: 30px; background-repeat: repeat; background-image: url('{{ asset('public/img/kebutuhan/keropi.jpg') }}');">
    <div style="border: 5px solid black; padding: 30px -10px 30px 30px; border-radius: 30px; padding: 30px; background-color: rgba(0, 0, 0, 0.8); color: white;">
        <table style="width: 100%; opacity: 1;">
            <tbody>
                <tr>
                    <td class="headerKiri" colspan="4">@KFSKIN.SAMARINDA</td>
                    <td class="headerKanan" colspan="4">Samarinda, {{ date('j F Y', strtotime($data['created_at'])) }}</td>
                </tr>
                <tr>
                    <td class="headerKiri" colspan="4">{{ $data['users']->name }}</td>
                    <td class="headerKanan" colspan="4">Kepada, {{ $data['customer']->nama }}</td>
                </tr>
                <tr>
                    <td style="padding-bottom: 50px " class="headerKiri" colspan="4">Samarinda</td>
                    <td  style="padding-bottom: 50px " class="headerKanan" colspan="4">{{ $data['customer']->alamat }}</td>
                </tr>
                <tr>
                    <th class="pt" style="padding-left: 20px;">Qty</th>
                    <th class="pt" colspan="2">Nama Produk</th>
                    <th class="pt" colspan="2">Harga Satuan</th>
                    <th class="pt" colspan="2">Jumlah</th>
                </tr>
                @foreach ($data['rinci'] as $rinci)
                    @php
                        $total += $rinci->harga*$rinci->qty;
                        $tinggi += 3;
                    @endphp
                    <tr>
                        <td class="" style="padding-left: 25px;">{{ $rinci->qty }}</td>
                        <td class="" colspan="2">{{ $rinci->produk->nama }}</td>
                        <td class="" colspan="2">Rp. {{ $rinci->harga }}</td>
                        <td class="" colspan="2">Rp. {{ $rinci->harga*$rinci->qty }}</td>
                    </tr>
                @endforeach
                <tr class="">
                    <td colspan="5"></td>
                    <th style="padding-bottom: 10px;">
                        <img src="{{ asset('public/img/kebutuhan/tanda.png') }}" alt="" srcset="" width="130px">
                        {{-- <hr style="display: inline-block; background-color: white; height: 3px; width: 120px; margin: -10px 0 0 0;">
                        <span style="display: inline-block; font-size: 20pt;">&#43;</span> --}}
                        <span style="display: block;">Rp. {{ $total }}</span>
                    </th>
                </tr>
                <tr>
                    <td colspan="7" style="background-color: black; padding: 5px;">

                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td colspan="2">
                        <div style="border: 2px white solid; text-align: center; width: 75%; font-size: 10pt;">
                            Perhatian...!!! <br>
                            Barang yang sudah dibeli tidak dapat ditukar/
                            dikembalikan lagi kecuali ada perjanjian
                        </div>
                    </td>
                    <td colspan="2" style="text-align: center;">
                        <span>Hormat Kami,</span> <br>
                        <img src="{{ asset('public/img/kebutuhan/lunas.png') }}" alt="" style="width: 150px; z-index: 999; transform: rotate(-<?= rand(5,30) ?>deg);"> <br>
                        <span>{{ $data['users']->name }}</span>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    </div>
    {{-- <div class="container">
        <div class="row">
            <div class="col-md-8 text-sm">
                <div class="row">
                    <div class="col">KF Skin</div>
                </div>
                <div class="row">
                    <div class="col">{{ $data['users']->name }}</div>
                </div>
                <div class="row">
                    <div class="col">Samarinda</div>
                </div>
            </div>
            <div class="col-md-4 text-sm">
                <div class="row">
                    <div class="col">Samarinda, {{ date('j F Y', strtotime($data['created_at'])) }}</div>
                </div>
                <div class="row">
                    <div class="col">Kepada, {{ $data['customer']->nama }}</div>
                </div>
                <div class="row">
                    <div class="col">{{ $data['customer']->alamat }}</div>
                </div>
            </div>
        </div>

        <div class="row mt-4">
            <div class="col">
                <div class="row">
                    <div class="col-sm-2">Qty</div>
                    <div class="col-sm-6">Nama Produk</div>
                    <div class="col-sm-2">Harga Satuan</div>
                    <div class="col-sm-2">Jumlah</div>
                </div>
                @foreach ($data['rinci'] as $rinci)
                    @php
                        $total += $rinci->harga*$rinci->qty
                    @endphp
                    <div class="row text-sm">
                        <div class="col-sm-2">{{ $rinci->qty }}</div>
                        <div class="col-sm-6">{{ $rinci->produk->nama }}</div>
                        <div class="col-sm-2">Rp. {{ $rinci->harga }}</div>
                        <div class="col-sm-2">Rp. {{ $rinci->harga*$rinci->qty }}</div>
                    </div>
                @endforeach
                <div class="row">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-6"></div>
                    <div class="col-sm-2"></div>
                    <div class="border-top" style="width: 100px; padding-left: 15px;">
                        <div class="row">
                            <div class="col text-sm">
                                <b>Rp. {{ $total }}</b>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}
</body>
</html>
