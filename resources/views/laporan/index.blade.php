@extends('layouts.template')
@section('title', 'Laporan | Admin')
@if ($_GET['laporan'] == 'bulanan')
    @section('contentHeader', 'Laporan Bulanan')
@else
    @section('contentHeader', 'Laporan Tahunan')
@endif
@section('breadCrumb')
    <li class="breadcrumb-item active">Laporan</li>
@endsection

@section('content')
{{-- content --}}
<div class="container-fluid">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-lg-12 bg-gray disabled rounded">
          <div class="card-body">
              {!! Form::open(['method' => 'POST', 'action' => 'App\Http\Controllers\LaporanController@store']) !!}
              @csrf
              @include('laporan.form')
          </div>
      </div>
    </div>
    <!-- /.row (main row) -->
  </div><!-- /.container-fluid -->
    {{-- endcontent --}}
@endsection
