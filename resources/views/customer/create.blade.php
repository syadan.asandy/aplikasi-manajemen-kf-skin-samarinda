@extends('layouts.template')
@section('title', 'Tambah Customer | Admin')
@section('contentHeader', 'Tambah Customer')
@section('breadCrumb')
    <li class="breadcrumb-item"><a href="{{ route('customer.index') }}">Data Customer</a></li>
    <li class="breadcrumb-item active">Tambah Customer</li>
@endsection

@section('content')
<!-- Main content -->
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-12 bg-gray disabled rounded">
            <div class="card-body">
                {!! Form::open(['method' => 'POST', 'action' => 'App\Http\Controllers\CustomerController@store']) !!}
                @csrf
                @include('customer.form')
            </div>
        </div>
      </div>
      <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
  <!-- /.content -->
@endsection
