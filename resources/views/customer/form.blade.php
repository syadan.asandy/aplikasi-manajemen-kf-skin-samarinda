<div class="form-group">
    {!! Form::label('nama', 'Nama: ') !!}
    {!! Form::text('nama', null, ['class' => 'form-control']) !!}
    @if ($errors->has('nama'))
        <span class="text-danger">{{ $errors->first('nama') }}</span>
    @endif
</div>
<div class="form-group">
    {!! Form::label('telepon', 'Telepon: ') !!}
    {!! Form::text('telepon', null, ['class' => 'form-control', 'onkeypress' => 'return angka(event)', 'id' => 'telepon']) !!}
    @if ($errors->has('telepon'))
    <span class="text-danger">Nomor telepon min 11 dan max 13 digit</span>
    @endif
</div>
<div class="form-group">
    {!! Form::label('kabupaten', 'Kabupaten: ') !!}
    <select name="kabupaten" id="kabupaten" class="form-control">
        <option value="">Pilih Kabupaten</option>
        @foreach ($kabupaten as $item)
            <option value="{{ $item->id }}">{{ $item->nama }}</option>
        @endforeach
    </select>
    @if ($errors->has('kelurahan'))
        <span class="text-danger">{{ $errors->first('kelurahan') }}</span>
    @endif
</div>
<div class="form-group" id="kelurahan" style="display: none">
    {!! Form::label('kelurahan', 'Kelurahan: ') !!}
    <select name="kelurahan" id="" class="form-control">
        @foreach ($kelurahan as $item)
            @if (isset($data->kelurahan_id))
                @if ($data->kelurahan_id == $item->id)
                    <option value="{{ $item->id }}" selected>{{ $item->nama }}</option>
                @endif
            @endif
            <option value="{{ $item->id }}">{{ $item->nama }}</option>
        @endforeach
    </select>
    @if ($errors->has('kelurahan'))
        <span class="text-danger">{{ $errors->first('kelurahan') }}</span>
    @endif
</div>
<div class="form-group" id="kelurahan1" style="display: none">
    {!! Form::label('kelurahan', 'Kelurahan: ') !!}
    <select name="kelurahan" id="" class="form-control">
        @foreach ($kelurahan1 as $item)
            @if (isset($data->kelurahan_id))
                @if ($data->kelurahan_id == $item->id)
                    <option value="{{ $item->id }}" selected>{{ $item->nama }}</option>
                @endif
            @endif
            <option value="{{ $item->id }}">{{ $item->nama }}</option>
        @endforeach
    </select>
    @if ($errors->has('kelurahan'))
        <span class="text-danger">{{ $errors->first('kelurahan') }}</span>
    @endif
</div>
<div class="form-group">
    {!! Form::label('alamat', 'Alamat: ') !!}
    {!! Form::text('alamat', null, ['class' => 'form-control']) !!}
    @if ($errors->has('alamat'))
        <span class="text-danger">{{ $errors->first('alamat') }}</span>
    @endif
</div>
{{-- {!! Form::hidden('user_id', Auth::user()->id, ['class' => 'form-control']) !!} --}}
{!! Form::submit($submitbuttontext, ['class' => 'btn btn-md btn-success']) !!}
{!! Form::close() !!}

@section('script')
    <script>
        $(document).ready(function(){
            $("select#kabupaten").change(function(){
                var selected = $(this).children("option:selected").val();
                switch (selected) {
                    case '1':
                        $('#kelurahan').css({'display': 'block'});
                        $('#kelurahan1').css({'display': 'none'});
                        break;
                    case '2':
                        $('#kelurahan').css({'display': 'none'});
                        $('#kelurahan1').css({'display': 'block'});
                        break;

                    default:
                        break;
                }
            });
        });
    </script>
@endsection
