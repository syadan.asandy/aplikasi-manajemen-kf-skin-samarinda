@if ($jenis == 'produk')
    @section('title', 'Sampah Produk | Admin')
    @section('contentHeader', 'Sampah Produk')
@else
    @section('title', 'Sampah Customer | Admin')
    @section('contentHeader', 'Sampah Customer')
@endif

@extends('layouts.template')
@section('breadCrumb')
    <li class="breadcrumb-item active">Data Sampah</li>
@endsection

@section('content')
{{-- content --}}
    <div class="container-fluid bg-secondary rounded">
    <!-- Small boxes (Stat box) -->
      {{-- <div class="row pt-4">
          <div class="col-lg-6">
            <form action="{{ route('laporan.update', $laporan) }}" method="post">
                @csrf
                @method('PUT')
                @if (isset($angkabulan))
                    {!! Form::hidden('bulan', $angkabulan, null) !!}
                @endif
                {!! Form::hidden('tahun', $tahun, null) !!}

                <button type="submit" class="btn btn-md btn-success">
                    <i class="fa fas fa-file-export">Unduh</i>
                </button>
            </form>
          </div>
      </div> --}}
      <div class="row mt-3 p-2">
        <div class="col-lg-12 p-4 bg-light rounded">
            <table id="tableData" class="table table-striped table-hover table-bordered dt-responsive nowrap" style="width:100%">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        @if ($jenis == 'customer')
                            <th>Telepon</th>
                            <th>Alamat</th>
                        @else
                            <th>Harga Beli</th>
                            <th>Harga Jual</th>
                        @endif
                        <th>Tanggal Hapus</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $index => $each)
                        <tr>
                            <td>{{ $index+1 }}</td>
                            <td>{{ $each->nama }}</td>
                            @if ($jenis == 'customer')
                                <td>{{ $each->telepon }}</td>
                                <td>{{ $each->alamat }}</td>
                            @else
                                <td>Rp. {{ $each->harga_beli }}</td>
                                <td>Rp. {{ $each->harga }}</td>
                            @endif
                            <td>{{ date('d-m-Y', strtotime($each->deleted_at)) }}</td>
                            <td>
                                <a href="{{ route('restore.sampah', ['jenis' => $jenis, 'id' => $each->id]) }}" class="btn btn-info">
                                    <i class="fa fas fa-undo"></i>
                                    Restore
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
      </div>
    </div><!-- /.container-fluid -->
{{-- endcontent --}}
@endsection
