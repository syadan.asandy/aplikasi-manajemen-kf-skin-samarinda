<div class="form-group">
    {!! Form::label('nama', 'Nama: ') !!}
    {!! Form::text('nama', null, ['class' => 'form-control', 'required']) !!}
    @if ($errors->has('nama'))
        <span class="text-danger">{{ $errors->first('nama') }}</span>
    @endif
</div>
<div class="form-group">
    {!! Form::label('harga_beli', 'Harga Beli: ') !!}
    {!! Form::text('harga_beli', null, ['class' => 'form-control', 'required', 'id' => 'harga_beli', 'onkeypress' => 'return angka(event)']) !!}
    @if ($errors->has('harga_beli'))
        <span class="text-danger">{{ $errors->first('harga_beli') }}</span>
    @endif
</div>
<div class="form-group">
    {!! Form::label('harga', 'Harga Jual: ') !!}
    {!! Form::text('harga', null, ['class' => 'form-control', 'required', 'id' => 'harga_jual', 'onkeypress' => 'return angka(event)']) !!}
    @if ($errors->has('harga'))
        <span class="text-danger">{{ $errors->first('harga') }}</span>
    @endif
</div>
{{-- <div class="form-group">
    {!! Form::label('foto', 'Foto: ') !!} <br>
    {!! Form::file('foto', null, ['class' => 'form-control']) !!}
</div> --}}
{{-- {!! Form::hidden('user_id', Auth::user()->id, ['class' => 'form-control']) !!} --}}
{!! Form::submit($submitbuttontext, ['class' => 'btn btn-md btn-success']) !!}
{!! Form::close() !!}
