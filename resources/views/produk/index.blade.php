@extends('layouts.template')
@section('title', 'Data Produk | Admin')
@section('contentHeader', 'Data Produk')
@section('breadCrumb')
    <li class="breadcrumb-item active">Data Produk</li>
@endsection

@section('content')
{{-- content --}}
    <div class="container-fluid bg-secondary rounded">
    <!-- Small boxes (Stat box) -->
      <div class="row pt-4">
          <div class="col-lg-6">
            <a href="{{ route('produk.create') }}" class="btn btn-md btn-primary">Tambah Produk</a>
          </div>
      </div>
      <div class="row mt-3 p-2">
        <div class="col-lg-12 p-4 bg-light rounded">
            <table id="tableData" class="table table-striped table-hover table-bordered dt-responsive nowrap" style="width:100%">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        {{-- <th>Foto</th> --}}
                        <th>Harga Beli</th>
                        <th>Harga Jual</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $index => $each)
                        <tr>
                            <td>{{ $index+1 }}</td>
                            <td>{{ $each->nama }}</td>
                            {{-- <td>{{ $each->foto }}</td> --}}
                            <td>Rp. {{ number_format($each->harga_beli, 0, ',', '.') }}</td>
                            <td>Rp. {{ number_format($each->harga, 0, ',', '.') }}</td>
                            <td>
                                <a href="{{ route('produk.edit', $each->slug) }}" class="btn btn-sm btn-warning">
                                    <i class="fas fa-pen"></i>
                                </a>
                                <form action="{{ route('produk.destroy', $each->slug) }}" method="post" style="display:inline">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-sm btn-danger btn-delete">
                                        <i class="fas fa-trash"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
      </div>
    </div><!-- /.container-fluid -->
{{-- endcontent --}}
@endsection
