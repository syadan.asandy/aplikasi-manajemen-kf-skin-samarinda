@extends('layouts.template')
@section('title', 'Tambah Produk | Admin')
@section('contentHeader', 'Tambah Produk')
@section('breadCrumb')
    <li class="breadcrumb-item"><a href="{{ route('produk.index') }}">Data Produk</a></li>
    <li class="breadcrumb-item active">Tambah Data</li>
@endsection

@section('content')
<!-- Main content -->
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-12 bg-gray disabled rounded">
            <div class="card-body">
                {!! Form::open(['method' => 'POST', 'action' => 'App\Http\Controllers\ProdukController@store', 'id' => 'kelas_create_form']) !!}
                @csrf
                @include('produk.form')
            </div>
        </div>
      </div>
      <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
  <!-- /.content -->
@endsection

@section('script')
    <script>
        var harga_beli = document.getElementById('harga_beli');
        harga_beli.addEventListener('keyup', function(e) {
            harga_beli.value = formatRupiah(this.value);
        });
        var harga_jual = document.getElementById('harga_jual');
        harga_jual.addEventListener('keyup', function(e) {
            harga_jual.value = formatRupiah(this.value);
        });
    </script>
@endsection
