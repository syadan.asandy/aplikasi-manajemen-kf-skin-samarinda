@extends('layouts.template')
@section('title', 'Ubah Ongkir | Admin')
@section('contentHeader', 'Ubah Ongkir')
@section('breadCrumb')
    <li class="breadcrumb-item"><a href="{{ route('ongkir.index') }}">Data Ongkir</a></li>
    <li class="breadcrumb-item active">Ubah Data</li>
@endsection

@section('content')
<!-- Main content -->
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-12 bg-gray disabled rounded">
            <div class="card-body">
                {!! Form::model($data, ['method' => 'PATCH', 'action' => ['App\Http\Controllers\OngkirController@update',$data->nama], 'id' => 'kelas_create_form']) !!}
                {{-- @csrf --}}
                @include('ongkir.form')
            </div>
        </div>
      </div>
      <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
  <!-- /.content -->
@endsection

@section('script')
    <script>
        var ongkir = document.getElementById('ongkir');
        ongkir.addEventListener('keyup', function(e) {
            ongkir.value = formatRupiah(this.value);
        });
    </script>
@endsection
