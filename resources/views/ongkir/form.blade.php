<div class="form-group">
    {!! Form::label('kabupaten_id', 'Kabupaten: ') !!}
    <select name="kabupaten_id" id="" class="form-control" disabled>
        <option value="{{ $data->kabupaten_id }}" selected>{{ $data->kabupaten->nama }}</option>
    </select>
    @if ($errors->has('kabupaten_id'))
        <span class="text-danger">{{ $errors->first('kabupaten_id') }}</span>
    @endif
</div>
<div class="form-group">
    {!! Form::label('nama', 'Nama: ') !!}
    {!! Form::text('nama', null, ['class' => 'form-control', 'required', 'disabled']) !!}
    @if ($errors->has('nama'))
        <span class="text-danger">{{ $errors->first('nama') }}</span>
    @endif
</div>
<div class="form-group">
    {!! Form::label('ongkir', 'Ongkos Kirim: ') !!}
    {{-- {!! Form::text('ongkir', null, ['class' => 'form-control', 'required']) !!} --}}
    <input type="text" name="ongkir" id="ongkir" class="form-control" required onkeypress="return angka(event)" value="{{ number_format($data->ongkir, 0, '.','.') }}">
    @if ($errors->has('ongkir'))
        <span class="text-danger">{{ $errors->first('ongkir') }}</span>
    @endif
</div>
{{-- <div class="form-group">
    {!! Form::label('foto', 'Foto: ') !!} <br>
    {!! Form::file('foto', null, ['class' => 'form-control']) !!}
</div> --}}
{{-- {!! Form::hidden('user_id', Auth::user()->id, ['class' => 'form-control']) !!} --}}
{!! Form::submit($submitbuttontext, ['class' => 'btn btn-md btn-success']) !!}
{!! Form::close() !!}
